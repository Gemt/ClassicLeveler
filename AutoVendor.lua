-- Author      : G3m7
-- Create Date : 5/5/2019 9:54:09 AM

function CLGuide_PrintQuestCompleteVendorCache()
    for i=1, getn(CLGuide_QuestCompleteVendorCache) do
        GuidePrint(CLGuide_QuestCompleteVendorCache[i])
    end
end

local function GetItemnameFromLink(link)
    local x = string.find(link, "%[") +1;
    local y = string.find(link, "%]", x) -1;
    return string.sub(link, x, y)
end

-- returns index of item in CLGuide_QuestCompleteVendorCache if it exist, otherwise 0
local function IsInQuestCompleteVendorCache(itemName)
    for i=1, getn(CLGuide_QuestCompleteVendorCache) do
        if itemName == CLGuide_QuestCompleteVendorCache[i] then return i end
    end
    return 0
end

local function LogSelling(value, firstRun)
    if firstRun == 0 then return end
    GuideWarning(value)
end

local function CLGuide_TryVendorBagItem(bag, slot, firstRun)
    local link = GetContainerItemLink(bag, slot)
    if link == nil then return 0 end

    -- if item is locked, it probably cant be vendored
    local _, _, locked = GetContainerItemInfo(bag, slot)
    
    -- actually, we dont want to return on locked, we want to pretend
    -- we're vendoring it if we are meant to, so the new OnUpdate logic
    -- wont receive 0 items vendored and continue to next state
    --if locked == 1 then return 0 end -- does it return 1/0 or true/false on classic?

    -- "ff9d9d9d" is the colorcode of a grey item
    local isGreyItem = (string.find(link, "ff9d9d9d") ~= nil)
    
    
    local itemName = GetItemnameFromLink(link)
    if isGreyItem and CLGuide_Options.AutoVendorGreyItems == true then
        if CLGuide_VendorWhiteList == nil or CLGuide_VendorWhiteList[itemName] == nil then
            LogSelling("Selling grey: "..GetContainerItemLink(bag, slot), firstRun)
            UseContainerItem(bag,slot)
            return 1
        end
    elseif CLGuide_Options.UseAutoVendorList == true then
        local vendorCacheIdx = IsInQuestCompleteVendorCache(itemName)
        if vendorCacheIdx > 0 then
            LogSelling("Selling from vendor cache: "..GetContainerItemLink(bag, slot), firstRun)
            table.remove(CLGuide_QuestCompleteVendorCache, vendorCacheIdx)
            UseContainerItem(bag,slot)
            return 1
        elseif CLGuide_VendorList ~= nil and CLGuide_VendorList[itemName] ~= nil and CLGuide_VendorList[itemName] <= UnitLevel("player") then
            LogSelling("Selling from vendor list: "..GetContainerItemLink(bag, slot), firstRun)
            UseContainerItem(bag,slot)
            return 1
        end
    end
    return 0
end

function CLGuide_SellItems(firstRun)
    local totalVendored = 0
    local bagslots = nil
    for bag=0,4 do
        local bagname = GetBagName(bag)
        if bagname ~= nil then
            bagslots = GetContainerNumSlots(bag)
            if bagslots and bagslots > 0 then
                for slot=1,bagslots do
                    totalVendored = totalVendored + CLGuide_TryVendorBagItem(bag, slot, firstRun)
                end
            end
        end
    end
    return totalVendored
end