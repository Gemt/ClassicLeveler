Relevant things to change/personalize can be found in 
Guides/
AutomaticSetup/

The Guides/ folder contains all the guides. When adding/removing guides you must keep these things in mind:
Each lua file represents a sections (with the exception of Guides/GuideSteps.lua). 
These section files must contain a globally unique section table (See any of the existing guides for reference)
The format for a section table is:
```CLGuide_myGLoballyUniqueName = {
    Title="Any title, this is shown in-game",
    PinBoard={"Things you want", "added to the pinboard", "when section is started"},
    Steps = {
        {Text="Accept The Balance of Nature", At="The Balance of Nature", point={x=5869,y=4427}},
		{Text="Complete {The Balance of Nature}", Ct="The Balance of Nature"},
        .... etc
    }
}
```

Each such section table (CLGuide_myGLoballyUniqueName) must be listed in Guides/GuideSteps.lua
Each section lua must also be listed in the Guides/includes.xml file.



For advanced automatic setup on login and skill training, see the files in AutomaticSetup/

AutomaticSetup/CVars.lua should list cvars you wish to be set automatically on login. See the file for documentation.
AutomaticSetup/Keybinds.lua allow you to specify keybinds which will be set on login. See the file for additional documentation
AutomaticSetup/Macros.lua allow you to define macros which will be automatically created when you login.
AutomaticSetup/Spells.lua defines spells you wish to train when you visit any trainer. This is primarily for class training, but can be used for any trainer.
AutomaticSetup/Talents.lua defines talents you wish to pick when you levelup. 
AutomaticSetup/VendorList.lua defines items you always wish to vendor when visiting a trainer.

Each of these advanced features has a config value to enable/disable them. They are currently all enabled by default,
but the files are empty, so nothing will be done until the tables are filled. 
The relevant config values can be found in Variables.lua. However, these features are not really something that makes
sense to change from ingame. It is something that should be configured before the game launches by setting the default
config values directly in Variables.lua