## Title: ClassicLeveler
## Version: 1.0
## Author: G3m7 & Hata
## Interface: 11302

##Dependencies: ClassicLeveler_Guide
##OptionalDeps: ClassicLeveler_AutomaticSetup

##SavedVariablesPerCharacter: CLGuide_CurrentStep, CLGuide_CurrentSection, CLGuide_Options, CLGuide_STVPages, CLGuide_ClothDonations, CLGuide_PinText

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\HereBeDragons\HereBeDragons-2.0.lua
Libs\HereBeDragons\HereBeDragons-Pins-2.0.lua
Libs\HereBeDragons\HereBeDragons-Migrate.lua

Dev.lua
Variables.lua
Utilities.lua

Triggers\includes.xml

UI\Arrow.lua
UI\Config.xml
UI\Config.lua
UI\PinBoard.xml
UI\PinBoard.lua
UI\ItemButton.xml
UI\ItemButton.lua
UI\GuideFrame.xml
UI\GuideFrame.lua
UI\GuideList.lua
UI\GuideList.xml

AutoVendor.lua
EventFrame.lua
AutomaticQuestHandler.lua
STVPageOverview.lua
ClothDonations.lua
FastLoot.lua