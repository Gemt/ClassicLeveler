--=========
--DEV STUFF
--=========
-- Put this anywhere you want to throw an error if the game CLGuide_GameVersion is not 1.12.x or 8.x
CLGuide_GameVersion = GetBuildInfo();
CLGuide_GameVersion = string.sub(CLGuide_GameVersion,1,4)
CLGuide_CurrentStep = {}
CLGuide_CurrentSection = {}

function Guide_PrintStepInfo()
    local step = CLGuide_CurrentStepTable--GuideSteps[CLGuide_Options"CurrentStep"]]
    if step == nil then
        GuidePrint("nil")
        return
    end
    
    GuidePrint("Section: "..CLGuide_CurrentSectionTable.Title.."("..CLGuide_CurrentSection..")")
    GuidePrint("Step: "..CLGuide_CurrentStep)
    GuidePrint("Text: "..step.Text)
    if step.At ~= nil then
        GuidePrint("AcceptQuestTrigger: "..step.At)
    end
    if step.Ct ~= nil then
        GuidePrint("CompletedTrigger: "..step.Ct)
    end
    if step.Dt ~= nil then
        --GuidePrint("DeliveredTrigger: "..step.Dt.q)
    end
    

end

-- Use this when printing warnings we wish to keep, even on launch
function GuideWarning(msg)
    if not DEFAULT_CHAT_FRAME then 
        return 
    end
    DEFAULT_CHAT_FRAME:AddMessage("|cffff0000"..msg)
end

function GuidePrint( msg )
    if not DEFAULT_CHAT_FRAME then 
        return 
    end
    DEFAULT_CHAT_FRAME:AddMessage ( msg )
    ChatFrame3:AddMessage ( msg )
    ChatFrame4:AddMessage ( msg )
end

local oldX, oldY = 0,0
function UpdateCoordBox()
    local x, y, m = HBD:GetPlayerZonePosition(true)-- = CLGuide_GetPlayerMapPosition("player")
    if not x or not y or not m then return end
    x = x*10000
    y = y*10000
    if x ~= oldX and y ~= oldY then
        oldX = x;
        oldY = y
        coordBox:SetText(", point={x="..string.format("%.0f", x)..",y="..string.format("%.0f", y)..",m="..tostring(m).."}")
    end
end

local function CLGuide_CreateMacro(name, icon, body, percharacter)
    if GetMacroIndexByName(name) == 0 then 
        if percharacter == 0 then
            CreateMacro(name, icon, body)
        else
            CreateMacro(name, icon, body, percharacter)
        end
    else 
        if percharacter == 0 then
            EditMacro(name, name, icon, body)
        else
            EditMacro(name, name, icon, body, percharacter)
        end
    end
end

function CLGuide_SetupMacros()
    GuideWarning("Starting creating macros")

    for i=1,getn(CLGuide_MacroTable) do
        local x = CLGuide_MacroTable[i];
        CLGuide_CreateMacro(x[1],x[2],x[3],x[4])
        if x[5] ~= nil and x[6] ~= nil and (GetActionText(x[5]) == nil or GetActionText(x[5]) ~= x[1]) then
            ClearCursor()
            PickupMacro(GetMacroIndexByName(x[1]))
            PlaceAction(x[5])
            ClearCursor()
        end
    end

    GuideWarning("Finished creating macros")
end