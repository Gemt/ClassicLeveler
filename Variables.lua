--=========
--Variables
--=========
CLGuide_DelayCheckHasQuest = 0
CLGuide_DelayCheckHasQuestStop = 0
CLGuide_CurrentStepTable = {}
CLGuide_CurrentSectionTable = {}
CLGuide_StepFrame = {}
CLGuide_FrameIsScaling = false
CLGuide_QuestCompleteVendorCache = {}

--==========
--Saved Vars
--==========
CLGuide_Options = {
    ["Rows"] = 5,                       -- how many guidesteps are shown 
    ["PreviousSteps"] = 1,              -- how many previous steps are shown (total rows are stil ["Rows"])
    ["FontSize"] = 14,                  --
    ["Locked"] = false,                 -- 
    ["PinBoardWidth"] = 400,            --
    ["Hide"] = false,
    ["Minimalist"] = false,

    ["AutoDeliverQuest"] = true,        --
    ["AutoAcceptQuest"] = true,         --
    ["AutoChooseQuestReward"] = false,   -- GuideSteps with Dt trigger will automatcally choose questreward when Dt contains Item="itemname"
    ["AutoVendorGreyItems"] = true,     --
    ["UseAutoVendorList"] = false,       -- When true, items from AutomaticSetup/VendorList.lua, and items from guideSteps marked as Vendor=1, will be vendored.
    ["EnableArrow"] = true,             -- when true, an arrow pointing towards the point of a guidestep will be shown
    ["EnableAutomaticTaxi"] = true,     -- guidesteps with Taxi="destination FP" will automatically fly to "desination FP" when talking to a taxi master
    ["EnableAutomaticBanking"] = true,  -- guidesteps with PutInBank={"itemname"} will place "itemname" in bank when talking to a banker
    ["AutoRepairMinLvl"] = 16,          -- At this lvl or higher autorepair is active.
                                        -- If set to 61 or higher, autorepair will not run.
    ["EnableFastLoot"] = true,          -- Enables automatic looting. Shift key disables this feature temporarily

    -- Advanced Setup
    ["EnableSpellTraining"] = true,     -- Will automatically train preconfigured spells when opening trainer. Requires AutomaticSetup/Spells.lua to be setup
    ["ShowTalentPicker"] = true,        -- A popupbutton will show on levelup to choose preconfigured talents from AutomaticSetup/Talents.lua
    
    -- Advanced Setup++ 
    ["SetupBinds"] = true,              -- Will run the AutomaticSetup/Keybinds.lua file on login
    ["SetupMacros"] = true,             -- Will run the AutomaticSetup/Macros.lua file on login
    ["SetupCVars"] = true,              -- Will run the AutomaticSetup/CVars.lua file on login
}

CLGuide_CurrentStep = 1
CLGuide_CurrentSection = 1

HBDPins = LibStub("HereBeDragons-Pins-2.0")
HBD = LibStub("HereBeDragons-2.0")