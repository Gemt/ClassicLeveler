-- Author      : G3m7
-- Create Date : 6/10/2019 5:11:03 PM

local Frame = CreateFrame("Frame")
Frame:RegisterEvent("QUEST_TURNED_IN")
CLGuide_ClothDonations = {
    [7802] = {req=12, type="Wool", who="Dwarves", done=0},
    [7807] = {req=12, type="Wool", who="Gnomes", done=0},
    [7792] = {req=12, type="Wool", who="Nelfs", done=0},
    [7791] = {req=12, type="Wool", who="Humans", done=0},
    
    [7803] = {req=26, type="Silk", who="Dwarves", done=0},
    [7808] = {req=26, type="Silk", who="Gnomes", done=0},
    [7798] = {req=26, type="Silk", who="Nelfs", done=0},
    [7793] = {req=26, type="Silk", who="Humans", done=0},

    [7809] = {req=40, type="Mageweave", who="Dwarves", done=0},
    [7804] = {req=40, type="Mageweave", who="Gnomes", done=0},
    [7799] = {req=40, type="Mageweave", who="Nelfs", done=0},
    [7794] = {req=40, type="Mageweave", who="Humans", done=0},

    [7811] = {req=50, type="Runecloth", who="Gnomes", done=0},
    [7805] = {req=50, type="Runecloth", who="Dwarves", done=0},
    [7800] = {req=50, type="Runecloth", who="Nelfs", done=0},
    [7795] = {req=50, type="Runecloth", who="Humans", done=0},
}

Frame:SetScript("OnEvent", function(self, event, ...)
    local questId, xp, money = ...
    local donation = CLGuide_ClothDonations[questId]
    if donation == nil then return end
    donation.done = 1
end)

function CLGuide_PrintClothDonationsStatus(msg)
    print("Level req: Wool: 12, Silk: 26, Mageweave: 40, Runecloth: 50")
    local lvl = UnitLevel("Player")
    
    local command = string.sub(msg, 7, string.len(msg))
    local all = (command=="all")
    
    local sorted = {}
    for i,n in pairs(CLGuide_ClothDonations) do
        if n.done == 0 and (all or (lvl > (n.req-5))) then
            table.insert(sorted, {req=n.req, type=n.type, who=n.who, done=n.done})
        end
    end
    table.sort(sorted, function(a,b)
        if a.req==b.req then
            return a.who<b.who
        else
            return a.req < b.req 
        end
    end)

    for i,n in ipairs(sorted) do
        GuidePrint(n.type..": "..n.who)
    end
end