
local EventFrame = CreateFrame("Frame")

EventFrame:RegisterEvent("CHAT_MSG_SYSTEM")
EventFrame:RegisterEvent("QUEST_COMPLETE")
EventFrame:RegisterEvent("QUEST_LOG_UPDATE")
--EventFrame:RegisterEvent("UNIT_QUEST_LOG_CHANGED") -- can we live with only QUEST_LOG_UPDATE?
EventFrame:RegisterEvent("CHAT_MSG_LOOT")
EventFrame:RegisterEvent("QUEST_ACCEPTED")
EventFrame:RegisterEvent("QUEST_DETAIL")
EventFrame:RegisterEvent("GOSSIP_SHOW")
EventFrame:RegisterEvent("QUEST_GREETING")
EventFrame:RegisterEvent("TAXIMAP_OPENED")
EventFrame:RegisterEvent("TAXIMAP_CLOSED")
EventFrame:RegisterEvent("MERCHANT_SHOW")
EventFrame:RegisterEvent("MERCHANT_CLOSED")
EventFrame:RegisterEvent("ZONE_CHANGED_NEW_AREA")
EventFrame:RegisterEvent("ZONE_CHANGED")
EventFrame:RegisterEvent("ZONE_CHANGED_INDOORS")
EventFrame:RegisterEvent("QUEST_PROGRESS")
EventFrame:RegisterEvent("TRAINER_SHOW")
EventFrame:RegisterEvent("TRAINER_CLOSED")
EventFrame:RegisterEvent("TRAINER_UPDATE")
EventFrame:RegisterEvent("CONFIRM_BINDER")
EventFrame:RegisterEvent("PLAYER_LEVEL_UP")
EventFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
EventFrame:RegisterEvent("CONFIRM_XP_LOSS")
EventFrame:RegisterEvent("PLAYER_DEAD")
EventFrame:RegisterEvent("PLAYER_XP_UPDATE")
EventFrame:RegisterEvent("BANKFRAME_OPENED")
EventFrame:RegisterEvent("CINEMATIC_START")
EventFrame:RegisterEvent("PLAYER_LOGIN")
EventFrame:RegisterEvent("BAG_UPDATE")
EventFrame:RegisterEvent("UNIT_INVENTORY_CHANGED")
EventFrame:RegisterEvent("LEARNED_SPELL_IN_TAB")
EventFrame:RegisterEvent("GOSSIP_CONFIRM_CANCEL")
EventFrame:RegisterEvent("CONFIRM_TALENT_WIPE")
EventFrame:RegisterEvent("CHARACTER_POINTS_CHANGED")
-- TODO: Remove for release, this is for debugging purposes
function CLGuide_RegisterAllEvents()
    EventFrame:RegisterAllEvents()    
end
CLGuide_PrintAllEvents = false
function CLGuide_PrintAllToggle()
    if CLGuide_PrintAllEvents == true then
        CLGuide_PrintAllEvents = false
    else 
        CLGuide_PrintAllEvents = true
    end
end

local function OnPlayerLogin()
    -- Class Step Removal
    local guideNumber = getn(CLGuide_GuideTable)
    local PlayerClass = UnitClass("PLAYER")
    for i = 1, guideNumber do
        local j = getn(CLGuide_GuideTable[i].Steps)
        for k in pairs(CLGuide_GuideTable[i].Steps) do
            if CLGuide_GuideTable[i].Steps[k].Class ~= nil then
                if CLGuide_GuideTable[i].Steps[k].Class ~= PlayerClass then
                    table.remove(CLGuide_GuideTable[i].Steps, k)
                end
            end
        end
    end
    
    --
    CLGuide_StepFrameRows(CLGuide_Options.Rows)
	--Hacky fix for Frame Scaling
	Guide_FrameIsScaling = true
	EventFrame.OnUpdate()
	Guide_FrameIsScaling = false
	--Set Step Stuff
	CLGuide_CurrentSectionTable = CLGuide_GuideTable[CLGuide_CurrentSection]
	CLGuide_SetStep(CLGuide_CurrentStep, 0)
	CLGuide_StepFrameSizer()
	CLGuide_Frame_GuideList:SetText(CLGuide_CurrentSectionTable.Title)
    CLGuide_PinBoard:SetWidth(CLGuide_Options.PinBoardWidth)
    CLGuide_Hide()
    CLGuide_Minimalist()
    CLGuide_Lock()
end

local function LogEvent(event, arg1, arg2, arg3, arg4)
    if arg4 ~= nil then 
		GuidePrint("EventFrame: "..event..", "..tostring(arg1)..", "..tostring(arg2)..", "..tostring(arg4)..", "..tostring(arg4)) 
    elseif arg3 ~= nil then 
	    GuidePrint("EventFrame: "..event..", "..tostring(arg1)..", "..tostring(arg2)..", "..tostring(arg3)) 
    elseif arg2 ~= nil then 
	    GuidePrint("EventFrame: "..event..", "..tostring(arg1)..", "..tostring(arg2)) 
	elseif arg1 ~= nil then 
		GuidePrint("EventFrame: "..event..", "..tostring(arg1)) 
	elseif event ~= nil then
		GuidePrint("EventFrame: "..event)
	end
end

local InitStuff = nil     -- Used for delayed initialization of UI and similar
local MerchantVisible = 0 -- 1 when merchant is visible, 0 otherwise
local IsAutovendoring = 0 -- When 1, autovendoring is in process and BuyItem trigger won't run until this is set to 0
local AutoVendorAttemptCount = 0
local NextVendorCheck = 0
local hasEnteredWorld = false
function EventFrame.OnEvent(self, event, arg1, arg2, arg3, arg4)
    if CLGuide_PrintAllEvents then
        LogEvent(event, arg1, arg2, arg3, arg4)
    end
    
    -- check MerchantVisible as well to prevent double-firing events from screwing 
    -- the rather sensitive states
    if event == "MERCHANT_SHOW" and MerchantVisible == 0 then 
        MerchantVisible = 1
        IsAutovendoring = 1
        AutoVendorAttemptCount = 0
        local totalVendored = CLGuide_SellItems(1)
        if totalVendored == 0 then
            IsAutovendoring = 0
            if UnitLevel("player") >= CLGuide_Options.AutoRepairMinLvl and CanMerchantRepair() == true then
                GuideWarning("Repairing all items")
                RepairAllItems()
            end
        else
            NextVendorCheck = GetTime() + 0.5
        end
    elseif event == "MERCHANT_CLOSED" then
        MerchantVisible = 0
    elseif event == "PLAYER_ENTERING_WORLD" then
        hasEnteredWorld = true
    elseif event == "PLAYER_LOGIN" then
        OnPlayerLogin()
        InitStuff = GetTime() + 2
    elseif event == "ZONE_CHANGED_NEW_AREA" or event == "ZONE_CHANGED" or event == "ZONE_CHANGED_INDOORS" then
        --CLGuide_BestMap = C_Map.GetBestMapForUnit("player")
    elseif event == "CINEMATIC_START" then
        CinematicFrame_CancelCinematic()
    end

    CLGuide_AcceptQuest(self, event, arg1)
    CLGuide_HandleBanking(self, event)
    CLGuide_BuyItem(self, event, arg1)
    CLGuide_CompleteQuest(self, event)
    CLGuide_CycleGossip(self, event)
    CLGuide_DeathWarp(self, event)
    CLGuide_DeliverQuest(self, event, arg1)
    CLGuide_HaveItem(self, event, arg1)
    CLGuide_HaveQuest(self, event)
    CLGuide_LevelReached(self, event, arg1)
    CLGuide_SetHs(self, event)
    CLGuide_TakeTaxi(self, event)
    CLGuide_TrainSkill(self, event)
    CLGuide_ZoneEntered(self, event)
    CLGuide_ItemButton_OnEvent(self, event, arg1)
    CLGuide_ResetTalents(self, event, arg1)
    if TalentPickerFrame_OnEvent ~= nil then
        TalentPickerFrame_OnEvent(self,event, arg1);
    end
end

EventFrame:SetScript("OnEvent", EventFrame.OnEvent)

-- See Guide_UnitQuestLogChanged documentation
local function CLGuide_DelayedCheckHasQuest()
	if DelayedCheckHasQuest == 1 then
		if CLGuide_CurrentStepTable.Ct ~= nil and CL_IsQuestComplete(CLGuide_CurrentStepTable.Ct) == 1 then
			CLGuide_CompleteCurrentStep()
		elseif CLGuide_CurrentStepTable.Dt ~= nil and CL_HasQuest(CLGuide_CurrentStepTable.At) == 0 then
			CLGuide_CompleteCurrentStep()
		elseif CLGuide_CurrentStepTable.Ht ~= nil and CL_HasQuest(CLGuide_CurrentStepTable.Ht) == 1 then
			CLGuide_CompleteCurrentStep()
		end
		-- If we have passed DelayedCheckHasQuestStop, stop looking
		if DelayedCheckHasQuestStop < GetTime() then
			DelayedCheckHasQuest = 0
		end
	end
end

local function InitializationOfStuff()
    if CLGuide_SetupKeybinds ~= nil and CLGuide_Options.SetupBinds == true then
        CLGuide_SetupKeybinds()
    end
    if CLGuide_MacroTable ~= nil and CLGuide_Options.SetupMacros == true then
        CLGuide_SetupMacros()
    end
    if CLGuide_SetupCVars ~= nil and CLGuide_Options.SetupCVars == true then
        CLGuide_SetupCVars()
    end
end

function EventFrame.OnUpdate()
    if InitStuff ~= nil and GetTime() > InitStuff and InCombatLockdown() ~= true and hasEnteredWorld == true then
        InitStuff = nil
        InitializationOfStuff()
    end

    if MerchantVisible == 1 and NextVendorCheck < GetTime() then
        NextVendorCheck = GetTime() + 1
        CLGuide_BuyItemOnUpdate()-- testing if we can safely run this together with vendoring to reduce the time it takes to vendor
        if IsAutovendoring == 1 and AutoVendorAttemptCount < 6 then
            AutoVendorAttemptCount = AutoVendorAttemptCount + 1
            local totalVendored = CLGuide_SellItems(0)
            if totalVendored == 0 then
                IsAutovendoring = 0
                if UnitLevel("player") >= CLGuide_Options.AutoRepairMinLvl and CanMerchantRepair() == true then
                    GuideWarning("Repairing all items")
                    RepairAllItems()
                end
            end
        else
            --CLGuide_BuyItemOnUpdate()
        end
    end


 	CLGuide_DelayedCheckHasQuest()
    CLGuide_CompleteQuestOnUpdate()
    CLGuide_TrainSkillOnUpdate()
    CLGuide_TaxiUpdate()
    CLGuide_ItemButton_OnUpdate()

    if CLGuide_CurrentStepTable.Proximity ~= nil then
        local pointDist = CLGuide_GetDistToActivePoint() 
        if pointDist ~= nil and pointDist < CLGuide_CurrentStepTable.Proximity then
            CLGuide_CompleteCurrentStep()
        end
    end
    
end

EventFrame:SetScript("OnUpdate", EventFrame.OnUpdate)

local function PrintHelp()
    print("ClassicLeveler (/cl, /clg, /classicleveler) Help:")
    print(string.format("%-15s%s", "cl hide",           "    Hides the guide frame"))
    print(string.format("%-15s%s", "cl show",           "   Shows the guide frame"))
    print(string.format("%-15s%s", "cl stv",            "     Prints missing STV Pages. When rescan, rescans bags"))
    print(string.format("%-15s%s\n%s", "cl cloth [all]",  "    Prints cloth donations not yet delivered.", 
        "                     Unless written as \'cl cloth all\', won't print\n                     quests where you are more than 5 lvls below req."))
    
    if CLGuide_SpellTable ~= nil then
        print(string.format("%-15s%s", "cl bars", "     Re-do actionbar setup according to AutomaticSetup/Spells table"))
    end
    print(string.format("%-15s%s", "cl mini",           "   removes all UI elements except guide steps"))
    print(string.format("%-15s%s", "cl options",           "   show options"))
    print(string.format("%-15s%s", "cl guides",           "   show guides"))
    print(string.format("%-15s%s", "cl lock",           "   lock the frame"))
end

SLASH_CLASSICLEVELER1, SLASH_CLASSICLEVELER2, SLASH_CLASSICLEVELER3 = '/cl', '/clg', '/classicleveler';
function SlashCmdList.CLASSICLEVELER(msg, editbox)
    if msg == nil then 
        PrintHelp()
        return 
    end
    if string.find(string.lower(msg), "stv") then
        PrintStvPageOverview()
    elseif string.find(string.lower(msg), "hide") then
        CLGuide_Options.Hide = true
        CLGuide_Hide()
    elseif string.find(string.lower(msg), "show") then
        CLGuide_Options.Hide = false
        CLGuide_Hide()
    elseif string.find(string.lower(msg), "cloth") then
        CLGuide_PrintClothDonationsStatus(msg)
    elseif string.find(string.lower(msg), "bars") then
        CLGuide_RePlaceSpells()
    elseif string.find(string.lower(msg), "mini") then
        if CLGuide_Options.Minimalist then
            CLGuide_Options.Minimalist=false
        else
            CLGuide_Options.Minimalist=true
        end
        CLGuide_Minimalist()
    elseif string.find(string.lower(msg), "options") then
        CLGuide_Config()
    elseif string.find(string.lower(msg), "guides") then
        CreateGuideList()
    elseif string.find(string.lower(msg), "lock") then
        if CLGuide_Options.Locked then
            CLGuide_Options.Locked=false
        else
            CLGuide_Options.Locked=true
        end
        CLGuide_Lock()
    else
        PrintHelp()
    end
end
