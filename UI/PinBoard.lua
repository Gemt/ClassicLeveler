local CLGuide_PinFrame = {}
local CLGuide_PinTextFrame = {}
local EmptyPinFrames = 0
local PinFrameAmount = 0
--Move this to Saved Vars
CLGuide_PinText = {}

--============================
-- Add & Remove Pins Functions
--============================
function CLGuide_AddPin(text)
	for k, v in pairs(CLGuide_PinText) do
		if v == text then 
			return
		end
	end

	if EmptyPinFrames > 0 then
		local useFrame = PinFrameAmount - EmptyPinFrames + 1
		for k, v in pairs(CLGuide_PinTextFrame) do
			if k == useFrame then 
				CLGuide_PinTextFrame[useFrame].text:SetText(text)
				CLGuide_PinTextFrame[useFrame]:Show()
				EmptyPinFrames = EmptyPinFrames - 1
			end
		end

	elseif EmptyPinFrames == 0 then
		local i = PinFrameAmount+1
		if i == 1 then
			CLGuide_PinTextFrame[i] = CreateFrame("FRAME", "CLGuide_PinTextFrame"..i, CLGuide_PinBoard)
			CLGuide_PinTextFrame[i]:SetPoint("TOP", CLGuide_PinBoard)
		else
			local offset = CLGuide_PinTextFrame[PinFrameAmount]:GetHeight()
			CLGuide_PinTextFrame[i] = CreateFrame("FRAME", "CLGuide_PinTextFrame"..i, CLGuide_PinTextFrame[PinFrameAmount]) 
			CLGuide_PinTextFrame[i]:SetPoint("TOP", CLGuide_PinTextFrame[PinFrameAmount], 0, -offset)
		end

		CLGuide_PinTextFrame[i].text = CLGuide_PinTextFrame[i]:CreateFontString(nil,"ARTWORK") 
		CLGuide_PinTextFrame[i].text:SetFont("Interface\\AddOns\\ClassicLeveler\\Artwork\\Inconsolata.ttf", CLGuide_Options.FontSize)
		CLGuide_PinTextFrame[i].text:SetPoint("TOPLEFT", CLGuide_PinTextFrame[i], "TOPLEFT", 2, -2)
		CLGuide_PinTextFrame[i].text:SetPoint("BOTTOMRIGHT", CLGuide_PinTextFrame[i], "BOTTOMRIGHT", 0, 2)
		CLGuide_PinTextFrame[i].text:SetJustifyV("TOP")
		CLGuide_PinTextFrame[i].text:SetJustifyH("LEFT")
		CLGuide_PinTextFrame[i].text:SetText(text)
		CLGuide_PinTextFrame[i]:SetWidth(CLGuide_Options.PinBoardWidth)
		CLGuide_PinTextFrame[i]:SetBackdrop{
				bgFile = "Interface\\BUTTONS\\WHITE8X8",
				insets = { left = 1, right = 1, top = 1, bottom = 1	 }
			}

		CLGuide_PinFrame[i] = CreateFrame("BUTTON", nil, CLGuide_PinTextFrame[i])
		CLGuide_PinFrame[i]:SetPoint("TOPLEFT", CLGuide_PinTextFrame[i], "TOPLEFT")
		CLGuide_PinFrame[i]:SetPoint("BOTTOMRIGHT", CLGuide_PinTextFrame[i], "BOTTOMRIGHT")
		CLGuide_PinFrame[i]:SetScript("OnClick", CLGuide_PinFrame.OnClick)
		CLGuide_PinFrame[i]:SetScript("OnMouseDown", CLGuide_PinFrame.Moving)
		CLGuide_PinFrame[i]:SetScript("OnDragStart", CLGuide_PinFrame.Moving)
		CLGuide_PinFrame[i]:SetScript("OnDragStop", CLGuide_PinFrame.StopMoving)
		CLGuide_PinFrame[i]:SetScript("OnMouseUp", CLGuide_PinFrame.StopMoving)

		CLGuide_PinTextFrame[i]:SetBackdropColor(0, 0, 0, 0.5)
		PinFrameAmount = PinFrameAmount + 1
	end

	CLGuide_PinBoard:Show()
	CLGuide_PinResize()
	table.insert(CLGuide_PinText, text)
end

function CLGuide_RemovePin(frameText)
	for k, v in pairs(CLGuide_PinText) do
		if v == frameText then 
			EmptyPinFrames = EmptyPinFrames + 1
			table.remove(CLGuide_PinText, k)
			local PinTextLength = getn(CLGuide_PinText)
			for i = 1, PinTextLength do
				CLGuide_PinTextFrame[i].text:SetText(CLGuide_PinText[i])
			end
			for i = PinTextLength+1, PinFrameAmount do
				CLGuide_PinTextFrame[i].text:SetText(" ")
				CLGuide_PinTextFrame[i]:Hide()
			end
			CLGuide_PinResize()
		end
	end
end

function CLGuide_PinResize()
	local h = 0
	local fsize = CLGuide_Options.FontSize
	if PinFrameAmount == EmptyPinFrames then 
		CLGuide_PinBoard:Hide()
	else 
		for i = 1, PinFrameAmount - EmptyPinFrames do
            CLGuide_PinTextFrame[i].text:SetWidth(CLGuide_PinTextFrame[i]:GetWidth())
            CLGuide_PinTextFrame[i]:SetHeight(CLGuide_PinTextFrame[i].text:GetStringHeight()+4)
			h = h + CLGuide_PinTextFrame[i]:GetHeight() + 1

			if i ~= 1 then
				local offset = CLGuide_PinTextFrame[i-1]:GetHeight() + 1
				CLGuide_PinTextFrame[i]:SetPoint("TOP", CLGuide_PinTextFrame[i-1], 0, -offset)
			end
		end
		CLGuide_PinBoard:SetHeight(h)
	end
end
--================
-- Mouse Functions
--================
function CLGuide_PinFrame.OnClick(self)
	if not IsShiftKeyDown() then 
		return 
	end
	local p = self:GetParent()
	CLGuide_RemovePin(p.text:GetText())
end

function CLGuide_PinFrame.Moving()
	CLGuide_PinBoard:StartMoving()
end

function CLGuide_PinFrame.StopMoving()
	CLGuide_PinBoard:StopMovingOrSizing()
end


