function CLGuide_Config_Save()
	CLGuide_Options.Rows=tonumber(CLGuide_ConfigFrame_Rows:GetText())
	CLGuide_Options.FontSize=tonumber(CLGuide_ConfigFrame_FontSize:GetText())
	CLGuide_Options.PreviousSteps=tonumber(CLGuide_ConfigFrame_PreviousSteps:GetText())

    CLGuide_Options.AutoDeliverQuest = CLGuide_ConfigFrame_AutoDeliverQuest:GetChecked()
    CLGuide_Options.AutoAcceptQuest = CLGuide_ConfigFrame_AutoAcceptQuest:GetChecked()
    CLGuide_Options.AutoChooseQuestReward = CLGuide_ConfigFrame_AutoQuestReward:GetChecked()
    
    CLGuide_Options.AutoVendorGreyItems = CLGuide_ConfigFrame_AutoVendorGrey:GetChecked()
    CLGuide_Options.UseAutoVendorList = CLGuide_ConfigFrame_UseAutoVendorList:GetChecked()

    CLGuide_Options.EnableAutomaticTaxi = CLGuide_ConfigFrame_AutoFly:GetChecked()
	
    CLGuide_Options.EnableArrow = CLGuide_ConfigFrame_EnableArrow:GetChecked()
    CLGuide_Options.EnableAutomaticBanking = CLGuide_ConfigFrame_EnableAutomaticBanking:GetChecked()
    CLGuide_Options.EnableFastLoot = CLGuide_ConfigFrame_EnableFastLoot:GetChecked()
    CLGuide_Options.AutoRepairMinLvl = tonumber(CLGuide_ConfigFrame_AutoRepairMinLvl:GetText())


    CLGuide_Options.EnableSpellTraining = CLGuide_ConfigFrame_EnableSpellTraining:GetChecked()
    CLGuide_Options.ShowTalentPicker = CLGuide_ConfigFrame_ShowTalentPicker:GetChecked()

    CLGuide_Options.SetupBinds = CLGuide_ConfigFrame_SetupBinds:GetChecked()
    CLGuide_Options.SetupMacros = CLGuide_ConfigFrame_SetupMacros:GetChecked()
    CLGuide_Options.SetupCVars = CLGuide_ConfigFrame_SetupCVars:GetChecked()
    ReloadUI()
end

