--------------------------------------------------------------
-- Frames used to display location of current step on map/minimap
--------------------------------------------------------------
local CLGuide_NextPointIconSizeMM = 6   -- size of current step location on minimap
local CLGuide_NextPointIconSizeWM = 10  -- size of current step location on worldmap
local numCreatedMapIcon = 0
local function CLGuide_CreateMapIconFrame(size)
    if size == 0 then return nil end
    numCreatedMapIcon = numCreatedMapIcon + 1
    local f = CreateFrame("Button", "CLGuide_MapFrameIcon"..numCreatedMapIcon, nil)
    f:SetFrameStrata("TOOLTIP")
    f:SetWidth(size)
    f:SetHeight(size)
    f.texture = f:CreateTexture(nil,"TOOLTIP")
    f.texture:SetTexture("Interface\\Minimap\\TRACKING\\Target.PNG")
    f.texture:SetWidth(size)
    f.texture:SetHeight(size)
    f.texture:SetAllPoints(f)
    f:SetPoint("CENTER",0,0)
    f:Hide()
    return f
end

local CLGuide_NextPointIconMM = CLGuide_CreateMapIconFrame(CLGuide_NextPointIconSizeMM)
local CLGuide_NextPointIconWM = CLGuide_CreateMapIconFrame(CLGuide_NextPointIconSizeWM)
local function CLGuide_RemoveMapIcons(mmf, wmf) 
    if mmf ~= nil then
        HBDPins:RemoveMinimapIcon("ClassicLeveler", mmf);
        mmf:Hide()
    end
    if wmf ~= nil then
        HBDPins:RemoveMinimapIcon("ClassicLeveler", wmf);
        wmf:Hide()
    end
end
local function CLGuide_PlaceMapIcon(mmf, wmf, point)
    if mmf ~= nil then
        mmf:ClearAllPoints()
        HBDPins:AddMinimapIconMap("ClassicLeveler", 
            mmf, 
            point.m,
            point.x/10000, 
            point.y/10000, 
            true, 
            true)
        mmf:Show()
    end
    if wmf ~= nil then
        wmf:ClearAllPoints()
        HBDPins:AddWorldMapIconMap("ClassicLeveler", 
            wmf, 
            point.m,
            point.x/10000, 
            point.y/10000, 
            HBD_PINS_WORLDMAP_SHOW_WORLD)
        wmf:Show()
    end
end
--------------------------------------------------------------

--=======================
--Frame Scripts & Buttons
--=======================
function CLGuide_Frame_OnLoad()
	--CLGuide_StepFrameRows(CLGuide_Options.Rows)
	--CLGuide_CurrentSectionTable = CLGuide_GuideTable[CLGuide_CurrentSection]
	--CLGuide_SetStep(CLGuide_CurrentStep, 0)
end

-- Called by all Triggers when a step completes automatically
-- Kept seperate from CLGuide_NextStep() in case we wish to 
-- handle automatic NextStep different from clicked NextStep
function CLGuide_CompleteCurrentStep()
    CLGuide_NextStep()
end

function CLGuide_NextSection()
    if CLGuide_CurrentSection == getn(CLGuide_GuideTable) then
        return
    else
        CLGuide_SetSection(CLGuide_CurrentSection + 1, 1)
    end
end

function CLGuide_PrevSection()
    if CLGuide_CurrentSection == 1 then
        return
    else
        local section = CLGuide_CurrentSection - 1
        local step = getn(CLGuide_GuideTable[section].Steps)
        CLGuide_SetSection(section, step)
    end
end

--These are run OnClick on Next / Previous Step Buttons
function CLGuide_NextStep()
	if CLGuide_CurrentStep == getn(CLGuide_CurrentSectionTable.Steps) then 
		GuidePrint("Guide Finished")
        CLGuide_NextSection()
		--CreateGuideList()
	else
		CLGuide_SetStep(CLGuide_CurrentStep + 1, 0)
	end
    CLGuide_StepHide()
    CLGuide_NextGuide()
end
function CLGuide_PrevStep()
	if CLGuide_CurrentStep > 1 then 
		CLGuide_SetStep(CLGuide_CurrentStep - 1, 1)
	else
        CLGuide_PrevSection()
    end
    CLGuide_StepHide()
    CLGuide_NextGuide()
end

function CLGuide_GuideList()
	if CLGuide_GuideListFrame:IsVisible() then 
		CLGuide_GuideListFrame:Hide()
	else 
		CLGuide_GuideListFrame:Show()
		CreateGuideList()
	end
end

function CLGuide_Config()
	if CLGuide_ConfigFrame:IsVisible() then
		CLGuide_ConfigFrame:Hide()
	else
		CLGuide_ConfigFrame:Show()
		CLGuide_ConfigFrame_Rows:SetText(CLGuide_Options.Rows)
		CLGuide_ConfigFrame_FontSize:SetText(CLGuide_Options.FontSize)
		CLGuide_ConfigFrame_PreviousSteps:SetText(CLGuide_Options.PreviousSteps)
        
        CLGuide_ConfigFrame_AutoDeliverQuest:SetChecked(CLGuide_Options.AutoDeliverQuest)
        CLGuide_ConfigFrame_AutoAcceptQuest:SetChecked(CLGuide_Options.AutoAcceptQuest)
        CLGuide_ConfigFrame_AutoQuestReward:SetChecked(CLGuide_Options.AutoChooseQuestReward)

        CLGuide_ConfigFrame_AutoVendorGrey:SetChecked(CLGuide_Options.AutoVendorGreyItems)
        CLGuide_ConfigFrame_UseAutoVendorList:SetChecked(CLGuide_Options.UseAutoVendorList)

        CLGuide_ConfigFrame_AutoFly:SetChecked(CLGuide_Options.EnableAutomaticTaxi)
        
        --
        CLGuide_ConfigFrame_EnableArrow:SetChecked(CLGuide_Options.EnableArrow)
        CLGuide_ConfigFrame_EnableAutomaticBanking:SetChecked(CLGuide_Options.EnableAutomaticBanking)
        CLGuide_ConfigFrame_EnableFastLoot:SetChecked(CLGuide_Options.EnableFastLoot)
        CLGuide_ConfigFrame_AutoRepairMinLvl:SetText(CLGuide_Options.AutoRepairMinLvl)


        CLGuide_ConfigFrame_EnableSpellTraining:SetChecked(CLGuide_Options.EnableSpellTraining)
        CLGuide_ConfigFrame_ShowTalentPicker:SetChecked(CLGuide_Options.ShowTalentPicker)

        CLGuide_ConfigFrame_SetupBinds:SetChecked(CLGuide_Options.SetupBinds)
        CLGuide_ConfigFrame_SetupMacros:SetChecked(CLGuide_Options.SetupMacros)
        CLGuide_ConfigFrame_SetupCVars:SetChecked(CLGuide_Options.SetupCVars)

	end
end

function CLGuide_Lock()
	if CLGuide_Options.Locked == true then
		CLGuide_Frame_ResizeButton:Hide()
        CLGuide_Frame:EnableMouse(false)
    else
        CLGuide_Frame_ResizeButton:Show()
        CLGuide_Frame:EnableMouse(true)
	end

end

function CLGuide_Hide()
    if CLGuide_Options.Hide then
        CLGuide_Frame:Hide()
        CLGuide_TomTomCrazyArrow:Hide()
    else
        CLGuide_Frame:Show()
        CLGuide_TomTomCrazyArrow:Hide()
    end
end

function CLGuide_Minimalist()
    if CLGuide_Options.Minimalist then
        CLGuide_Frame_PrevButton:Hide()
        CLGuide_Frame_NextButton:Hide()
        CLGuide_Frame_GuideList:Hide()
        CLGuide_Frame_Config:Hide()
        --coordBox:Hide()
    else
        CLGuide_Frame_PrevButton:Show()
        CLGuide_Frame_NextButton:Show()
        CLGuide_Frame_GuideList:Show()
        CLGuide_Frame_Config:Show()
        --coordBox:Show()
    end
end

--====================
--Guide Step Functions
--====================
function CLGuide_SetStep(step, wasPrevBtn)

	DelayedCheckHasQuest = 0 -- if step is changed, manual or otherwise, we stop any delayed hasQuest checking
	CLGuide_CurrentStep = step
	CLGuide_CurrentStepTable = CLGuide_CurrentSectionTable.Steps[step]

    -- Skip the step if it is a quest delivery trigger, the quest is not completed, and SkipIfUncomplete is set
    -- need the wasPrevBtn 1/0 so that if it WAS the PreviousStep button which called this function we ignore
    -- this logic so that it is possible to go back past the step
    if wasPrevBtn ~= 1 and CLGuide_CurrentStepTable.Dt ~= nil and CLGuide_CurrentStepTable.Dt.SkipIfUncomplete ~= nil then
        if CLGuide_IsQuestComplete(CLGuide_CurrentStepTable.Dt.q) == 0 then
            GuideWarning(CLGuide_CurrentStepTable.Dt.q.." > not completed, and SkipIfUncomplete flag set. Skipping step")
            CLGuide_NextStep()
            return
        end
    end

    if wasPrevBtn ~= 1 and CLGuide_CurrentStepTable.Dt ~= nil and CLGuide_CurrentStepTable.Dt.SkipIfNotHaveQuest ~= nil then
        if CLGuide_HaveQuestInQuestlog(CLGuide_CurrentStepTable.Dt.q) == 0 then
            GuideWarning(CLGuide_CurrentStepTable.Dt.q.." > not in qlog, and SkipIfNotHaveQuest flag set. Skipping step")
            CLGuide_NextStep()
            return
        end
    end

    

	--Set StepFrame text
	for i = 1, CLGuide_Options.Rows do 
		local StepOffset = step - CLGuide_Options.PreviousSteps + i - 1
		local s = CLGuide_CurrentSectionTable.Steps[StepOffset]
		if s ~= nil then 
			CLGuide_StepFrame[i].text:SetText(s.Text)
		else 
			CLGuide_StepFrame[i].text:SetText("")
		end
	end

	--Set Arrow. If no point is specified, this will instead hide the arrow.
	CLGuide_SetCrazyArrow(CLGuide_CurrentStepTable.point, CLGuide_CurrentStepTable.Text)
    
    -- check if any pins to add/remove
    if CLGuide_CurrentStepTable.PinAdd ~= nil then
        if type(CLGuide_CurrentStepTable.PinAdd) == "table" then
            for i=1,getn(CLGuide_CurrentStepTable.PinAdd) do
                CLGuide_AddPin(CLGuide_CurrentStepTable.PinAdd[i])
            end
        else
            CLGuide_AddPin(CLGuide_CurrentStepTable.PinAdd)
        end
    elseif CLGuide_CurrentStepTable.PinRemove ~= nil then
        CLGuide_RemovePin(CLGuide_CurrentStepTable.PinRemove)
    end

    if CLGuide_CurrentStepTable.UseItem ~= nil then
        CLGuide_SetupItemButton(CLGuide_CurrentStepTable.UseItem)
    end
    
    if CLGuide_CurrentStepTable.point and CLGuide_CurrentStepTable.point.m ~= nil then
        CLGuide_PlaceMapIcon(CLGuide_NextPointIconMM, CLGuide_NextPointIconWM, CLGuide_CurrentStepTable.point)
    else
        CLGuide_RemoveMapIcons(CLGuide_NextPointIconMM, CLGuide_NextPointIconWM)
    end
    
	--Guide_PrintStepInfo()
	CLGuide_UpdateColors()
end

function CLGuide_SetSection(sectionNum, step)
	CLGuide_CurrentSection = sectionNum
	CLGuide_CurrentSectionTable = CLGuide_GuideTable[sectionNum]
    if CLGuide_CurrentSectionTable.Pinboard ~= nil then
        for i=1, getn(CLGuide_CurrentSectionTable.Pinboard) do
            CLGuide_AddPin(CLGuide_CurrentSectionTable.Pinboard[i])
        end
    end
	CLGuide_SetStep(step, 0)
	CLGuide_Frame_GuideList:SetText(CLGuide_CurrentSectionTable.Title)
end

--The cheat solution that assumes previous steps to be completed
function CLGuide_UpdateColors()
	CLGuide_StepFrame[1+CLGuide_Options.PreviousSteps]:SetBackdropColor(0.3, 0.3, 0.3, 0.5)
	--If you want to actually check previous steps for completion, you can just add the check in the for loop
	if CLGuide_Options.PreviousSteps > 0 then 
		for i = 1, CLGuide_Options.PreviousSteps do 
			CLGuide_StepFrame[i]:SetBackdropColor(0.1, 0.3, 0.1, 0.5)
		end
	end
	CLGuide_StepFrameSizer()
end

function CLGuide_StepHide()
    if CLGuide_CurrentStepTable.Hide == true then 
        CLGuide_Options.Hide = true
        CLGuide_Hide()
    elseif CLGuide_CurrentStepTable.Hide == false then
        CLGuide_Options.Hide = false
        CLGuide_Hide()
    end
end

function CLGuide_NextGuide()
    if CLGuide_CurrentStepTable.NextGuide ~= nil then
        for i = 1, CLGuide_TableLength do
            if CLGuide_GuideTable[i].Title == CLGuide_CurrentStepTable.NextGuide then
                CLGuide_SetSection(i, 1)
                break
            end
        end
    end
end

--=====================
--Frame Creator & Sizer
--=====================
function CLGuide_StepFrameRows(rows)
	if rows > getn(CLGuide_StepFrame) then
		CLGuide_Options.Rows = rows
		for i = getn(CLGuide_StepFrame)+1, rows do 
			CLGuide_StepFrame[i] = CreateFrame("FRAME", "CLGuide_StepFrame"..i, CLGuide_Frame)
			CLGuide_StepFrame[i]:Show()
			--texture
			CLGuide_StepFrame[i]:SetBackdrop{
				bgFile = "Interface\\BUTTONS\\WHITE8X8",
				insets = { left = 1, right = 1, top = 1, bottom = 1	 }
			}
			CLGuide_StepFrame[i]:SetBackdropColor(0, 0, 0, 0.5)
			--Text 
			CLGuide_StepFrame[i].text = CLGuide_StepFrame[i]:CreateFontString(nil,"ARTWORK") 
			CLGuide_StepFrame[i].text:SetFont("Interface\\AddOns\\ClassicLeveler\\Artwork\\Inconsolata.ttf", CLGuide_Options.FontSize)
			CLGuide_StepFrame[i].text:SetPoint("TOPLEFT", CLGuide_StepFrame[i], "TOPLEFT", 10, -5)
			CLGuide_StepFrame[i].text:SetPoint("BOTTOMRIGHT", CLGuide_StepFrame[i], "BOTTOMRIGHT", -10, -5)
			CLGuide_StepFrame[i].text:SetJustifyV("TOP")
			CLGuide_StepFrame[i].text:SetJustifyH("LEFT")
			CLGuide_StepFrame[i].text:SetText("Step "..i)
		end
	elseif rows < getn(CLGuide_StepFrame) then
		CLGuide_Options.Rows = rows
		for i = rows+1, getn(CLGuide_StepFrame) do
			CLGuide_StepFrame[i]:Hide()
		end
	end
	CLGuide_UpdateColors()

    -- Create PinBoard on load
    local tblen = getn(CLGuide_PinText)
    local k = CLGuide_PinText
    CLGuide_PinText = {}
    for i = 1, tblen do
        CLGuide_AddPin(k[i])        
    end

end
--This is called OnUpdate when it is being resized
--Also run once at OnLoad because reasons
function CLGuide_StepFrameSizer()
	local FrameNumber = CLGuide_Options.Rows
	local h = CLGuide_Frame:GetHeight()
	for i = 1, FrameNumber do
		local topOffset = (i-1)*(h/FrameNumber)
		local bottomOffset = h-(i*(h/FrameNumber))
		CLGuide_StepFrame[i]:SetPoint("TOPLEFT", CLGuide_Frame, "TOPLEFT", 0, -topOffset)
		CLGuide_StepFrame[i]:SetPoint("BOTTOMRIGHT", CLGuide_Frame, "BOTTOMRIGHT", 0, bottomOffset)
	end
end

--========
--OnUpdate
--========

function CLGuide_Frame_OnUpdate() 
	if CLGuide_FrameIsScaling then 
		CLGuide_StepFrameSizer()
	end
	--UpdateCoordBox()
end

function CLGuide_Frame_OnMouseWheel(self, delta)
    if not IsAltKeyDown() then return end
    if delta > 0 then   -- up
        CLGuide_PrevStep()
    else                -- down
        CLGuide_NextStep()
    end

end