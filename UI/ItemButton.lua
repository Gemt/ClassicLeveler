-- SecureActionButtonTemplate https://code.google.com/archive/p/autobar/wikis/AutoBarTest.wiki
-- Read https://wowwiki.fandom.com/wiki/API_PickupContainerItem

function CLGuide_ItemButton_OnMouseDown()
    if IsAltKeyDown() then
        CLGuide_ItemButton:StartMoving()
        isMoving = true
    end
end
function CLGuide_ItemButton_OnMouseUp()
    if IsShiftKeyDown() and GetMouseFocus():GetName() == "CLGuide_ItemButton_Button" then
        CLGuide_ItemButton_Button.itemName = nil
        CLGuide_ItemButton:Hide()
        GameTooltip:Hide()
    end
    CLGuide_ItemButton:StopMovingOrSizing()
    isMoving = false
end

function CLGuide_ItemButton_OnEnter()
    -- sometimes we won't have the texture of an item at the moment the button is initialized,
    -- in such cases we can try to refresh the texture here
    if CLGuide_ItemButton_ButtonIcon:GetTexture() == "Interface\\Icons\\inv_misc_questionmark" and CLGuide_ItemButton_Button.itemName ~= nil then
        local texture = GetItemIcon(CLGuide_ItemButton_Button.itemName)
        if texture ~= nil then
            CLGuide_ItemButton_ButtonIcon:SetTexture(texture)
        end
    end


    if CLGuide_ItemButton:IsVisible() and CLGuide_ItemButton_Button.itemName ~= nil then
        local bag,slot = CLGuide_GetInventoryItemInfo(CLGuide_ItemButton_Button.itemName)
        if bag and slot then
            GameTooltip:SetOwner(CLGuide_ItemButton)
            GameTooltip:SetBagItem(bag,slot)
            return
        else
            GuideWarning("["..CLGuide_ItemButton_Button.itemName.."] - Item not found in bags")
        end
    end
end

function CLGuide_ItemButton_OnLeave()
    if CLGuide_ItemButton:IsVisible() and CLGuide_ItemButton_Button.itemName ~= nil then
        GameTooltip:Hide()
        return
    end
end

local ShowItemButton = nil
function CLGuide_SetupItemButton(itemName)
    ShowItemButton = itemName
    --CLGuide_ItemButton_Button.itemName = itemName
    --local texture = GetItemIcon(itemName) or "Interface\\Icons\\inv_misc_questionmark"
    --CLGuide_ItemButton_ButtonIcon:SetTexture(texture)
    --CLGuide_ItemButton_Button:SetAttribute("item", itemName)
    --CLGuide_ItemButton:Show()
end


function CLGuide_ItemButton_OnUpdate()
    if ShowItemButton == nil then return end
    if InCombatLockdown() == true then return end
    CLGuide_ItemButton_Button.itemName = ShowItemButton
    local texture = GetItemIcon(ShowItemButton) or "Interface\\Icons\\inv_misc_questionmark"
    CLGuide_ItemButton_ButtonIcon:SetTexture(texture)
    CLGuide_ItemButton_Button:SetAttribute("item", ShowItemButton)
    CLGuide_ItemButton:Show()
    ShowItemButton = nil
end

function CLGuide_ItemButton_OnEvent(self, event, arg1)
    if InCombatLockdown() == true then return end
    if CLGuide_ItemButton:IsShown() ~= true then return end
    if CLGuide_ItemButton_Button.itemName == nil then return end
    if event == "UNIT_INVENTORY_CHANGED" and arg1 == "player" then 
        for i = 1, 19 do
            local link = GetInventoryItemLink("player", i)
            if link ~= nil and string.find(link,CLGuide_ItemButton_Button.itemName) ~= nil then
                -- we equipped the item the button was used for, we can hide it
                CLGuide_ItemButton_Button.itemName = nil
                CLGuide_ItemButton:Hide()
                GameTooltip:Hide()
                return
            end
        end
    end
end