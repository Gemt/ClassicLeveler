local ListCreated = false
CLGuide_TableLength = getn(CLGuide_GuideTable)
local CLGuide_GuideListFrames = {}

function CreateGuideList()
	if ListCreated == false then 


		CLGuide_GuideListScrollFrame = CreateFrame("ScrollFrame", "CLGuide_GuideListScrollFrame", CLGuide_GuideListFrame, "UIPanelScrollFrameTemplate")
		CLGuide_GuideListScrollFrame:SetPoint("TOPLEFT", CLGuide_GuideListFrame, "TOPLEFT", 4, -8)
		CLGuide_GuideListScrollFrame:SetPoint("BOTTOMRIGHT", CLGuide_GuideListFrame, "BOTTOMRIGHT", -3, 4)

		local child = CreateFrame("Frame", nil, CLGuide_GuideListScrollFrame)
		child:SetHeight(40*12)
		child:SetWidth(CLGuide_GuideListFrame:GetWidth())
		CLGuide_GuideListScrollFrame:SetScrollChild(child)


		for i = 1, CLGuide_TableLength do
			CLGuide_GuideListFrames[i] = CreateFrame("Button", "ListCLGuide_GuideListFrames"..i, child)
	        CLGuide_GuideListFrames[i]:SetPoint("TOPLEFT", child, "TOPLEFT", 0, -40*(i-1))
	        CLGuide_GuideListFrames[i]:SetWidth(280)
	        CLGuide_GuideListFrames[i]:SetHeight(40)

	        CLGuide_GuideListFrames[i].text = CLGuide_GuideListFrames[i]:CreateFontString(nil,"ARTWORK") 
			CLGuide_GuideListFrames[i].text:SetFont("Interface\\AddOns\\ClassicLeveler\\Artwork\\Inconsolata.ttf", CLGuide_Options.FontSize)
			CLGuide_GuideListFrames[i].text:SetPoint("LEFT", CLGuide_GuideListFrames[i], "LEFT", 5, 0)

			CLGuide_GuideListFrames[i].text:SetText(CLGuide_GuideTable[i].Title)

			CLGuide_GuideListFrames[i]:SetScript("OnClick", GuideListFrames_OnClick)

			CLGuide_GuideListFrames[i]:SetBackdrop{
				bgFile = "Interface\\BUTTONS\\WHITE8X8",
				insets = { left = 1, right = 1, top = 1, bottom = 1	 }
			}
			CLGuide_GuideListFrames[i]:SetBackdropColor(0, 0, 0, 0.5)
		end

		


		ListCreated = true
	end
	CLGuide_GuideListFrame:SetHeight(40*13)
	CLGuide_GuideListFrame:Show()
end

function GuideListFrames_OnClick(self)
	local Guide = self.text:GetText()
	for i = 1, CLGuide_TableLength do
		if CLGuide_GuideTable[i].Title == Guide then
			CLGuide_SetSection(i, 1)
			CLGuide_GuideListFrame:Hide()
		end
	end

end

