----------------------------------------------------------------------------
--  TomTom: A navigational assistant for World of Warcraft
--  CrazyTaxi: A crazy-taxi style arrow used for waypoint navigation.
--  concept taken from MapNotes2 (Thanks to Mery for the idea, along
--  with the artwork.)
----------------------------------------------------------------------------

--Lua5 doesnt support mod math via the % operator
function CLGuide_Questie_Modulo(val, by)
    return val - math.floor(val/by)*by;
end

---------------------------------------------------------------------------------------------------
local sformat = string.format
local function ColorGradient(perc, ...)
	local num = select("#", ...)
	local hexes = type(select(1, ...)) == "string"

	if perc == 1 then
		return select(num-2, ...), select(num-1, ...), select(num, ...)
	end

	num = num / 3

	local segment, relperc = math.modf(perc*(num-1))
	local r1, g1, b1, r2, g2, b2
	r1, g1, b1 = select((segment*3)+1, ...), select((segment*3)+2, ...), select((segment*3)+3, ...)
	r2, g2, b2 = select((segment*3)+4, ...), select((segment*3)+5, ...), select((segment*3)+6, ...)

	if not r2 or not g2 or not b2 then
		return r1, g1, b1
	else
		return r1 + (r2-r1)*relperc,
		g1 + (g2-g1)*relperc,
		b1 + (b2-b1)*relperc
	end
end
local twopi = math.pi * 2


CLGuide_wayframe = CreateFrame("Button", "CLGuide_TomTomCrazyArrow", UIParent)
CLGuide_wayframe:SetHeight(42)
CLGuide_wayframe:SetWidth(56)
CLGuide_wayframe:SetPoint("BOTTOM", nil, "BOTTOM", 0, 185)
CLGuide_wayframe:EnableMouse(true)
CLGuide_wayframe:SetMovable(true)
-- Frame used to control the scaling of the title and friends
local titleframe = CreateFrame("Frame", nil, CLGuide_wayframe)
CLGuide_wayframe.title = titleframe:CreateFontString("OVERLAY", nil, "GameFontHighlightSmall")
CLGuide_wayframe.status = titleframe:CreateFontString("OVERLAY", nil, "GameFontNormalSmall")
CLGuide_wayframe.title:SetPoint("TOP", CLGuide_wayframe, "BOTTOM", 0, 0)
CLGuide_wayframe.status:SetPoint("TOP", CLGuide_wayframe.title, "BOTTOM", 0, 0)
---------------------------------------------------------------------------------------------------
local function OnDragStart(self, button)
    if IsControlKeyDown() and IsShiftKeyDown() then
        self:StartMoving()
        CLGuide_wayframe:SetClampedToScreen(true);
    end
end
---------------------------------------------------------------------------------------------------
local function OnDragStop(self, button)
    self:StopMovingOrSizing()
end
---------------------------------------------------------------------------------------------------
local function CLGuide_wayframe_OnClick(self, event, arg1)
    if (GetMouseFocus():GetName() == "CLGuide_TomTomCrazyArrow") then
        CLGuide_TomTomCrazyArrow:Hide()
    end
end

CLGuide_wayframe:SetScript("OnClick", CLGuide_wayframe_OnClick)
CLGuide_wayframe:RegisterForClicks("RightButtonUp")
---------------------------------------------------------------------------------------------------
CLGuide_wayframe:SetScript("OnDragStart", OnDragStart)
CLGuide_wayframe:SetScript("OnDragStop", OnDragStop)
CLGuide_wayframe:RegisterForDrag("LeftButton")
CLGuide_wayframe:RegisterEvent("ZONE_CHANGED_NEW_AREA")

CLGuide_wayframe.arrow = CLGuide_wayframe:CreateTexture("OVERLAY")
CLGuide_wayframe.arrow:SetTexture("Interface\\AddOns\\ClassicLeveler\\images\\Arrow")
CLGuide_wayframe.arrow:SetAllPoints()
---------------------------------------------------------------------------------------------------
local active_point, arrive_distance, showDownArrow, point_title, arrow_objective, isHide
local TestPoint = {
x = 324, 
y = 495
}

function CLGuide_SetCrazyArrow(point, title)
    if CLGuide_Options.EnableArrow == false then 
        CLGuide_wayframe:Hide()
        return 
    end

    active_point = point
    arrive_distance = 999999
    point_title = title
    if active_point and not isHide then
        CLGuide_wayframe.title:SetText(point_title or "Unknown waypoint")
        CLGuide_wayframe:Show()
		CLGuide_wayframe.arrow:Show()
    else
        CLGuide_wayframe:Hide()
    end
end
---------
local status = CLGuide_wayframe.status
local arrow = CLGuide_wayframe.arrow
local arrowLastUpdate = 0;
local LastPlayerPosition = {};
local count = 0

function CLGuide_GetDistToActivePoint()
	if active_point == nil then
		return nil
	end
	return arrive_distance
end
local goodcolor = {0, 0.9, 0}
local badcolor = {1, 0, 0}
local middlecolor = {1, 0.5, 0}
local exactcolor = {0, 1, 0}

local function OnUpdate(self, elapsed)
	if active_point == nil then
		return
	end
    if GetTime() - arrowLastUpdate >= .04 then
        arrowLastUpdate = GetTime()
        if CLGuide_TomTomCrazyArrow:IsVisible() == nil then return end

        local dist, angle
        local clgPointX = active_point.x/10000
        local clgPointY = active_point.y/10000
        if active_point.m ~= nil then
            local playerX, playerY, instId = HBD:GetPlayerWorldPosition()
            local pointX, pointY = HBD:GetWorldCoordinatesFromZone(clgPointX, clgPointY, active_point.m)
            dist = HBD:GetWorldDistance(instId, playerX, playerY, pointX, pointY)
            angle = HBD:GetWorldVector(instId, playerX, playerY, pointX, pointY)
        else
            -- compatibility with points without zone id 
            local playerX, playerY, instId = HBD:GetPlayerWorldPosition(true)
            local pointX, pointY = HBD:GetWorldCoordinatesFromZone(clgPointX, clgPointY, HBD:GetPlayerZone())
            dist = HBD:GetWorldDistance(instId, playerX, playerY, pointX, pointY)
            angle = HBD:GetWorldVector(instId, playerX, playerY, pointX, pointY)
        end

        arrive_distance = dist
        if not dist or IsInInstance() then
            if not active_point.x and not active_point.y then
                active_point = nil
            end
            self:Hide()
            return
        end
        status:SetText(sformat("%d yards", dist))
        if showDownArrow then
            arrow:SetHeight(56)
            arrow:SetWidth(42)
            arrow:SetTexture("Interface\\AddOns\\ClassicLeveler_Guide\\images\\Arrow")
            showDownArrow = false
        end
		local player = GetPlayerFacing()
		angle = angle - player
		local perc = math.abs((math.pi - math.abs(angle)) / math.pi)
        local gr,gg,gb = unpack(goodcolor)
		local mr,mg,mb = unpack(middlecolor)
		local br,bg,bb = unpack(badcolor)
		local r,g,b = ColorGradient(perc, br, bg, bb, mr, mg, mb, gr, gg, gb)
		-- If we're 98% heading in the right direction, then use the exact
		-- color instead of the gradient. This allows us to distinguish 'good'
		-- from 'on target'. Thanks to Gregor_Curse for the suggestion.
		if perc > 0.98 then
			r,g,b = unpack(exactcolor)
		end
		arrow:SetVertexColor(r,g,b)

		local cell = floor(angle / twopi * 108 + 0.5) % 108
		local column = cell % 9
		local row = floor(cell / 9)

		local xstart = (column * 56) / 512
		local ystart = (row * 42) / 512
		local xend = ((column + 1) * 56) / 512
		local yend = ((row + 1) * 42) / 512
		arrow:SetTexCoord(xstart,xend,ystart,yend)
        
    end
end
CLGuide_wayframe:SetScript("OnUpdate", OnUpdate)