-- Author      : G3m7
-- Create Date : 6/9/2019 1:34:22 PM

------------------------------------------
-- fast loot, leatrix_plus
------------------------------------------
local stvPages = {
["Green Hills of Stranglethorn - Page 1"] =1,
["Green Hills of Stranglethorn - Page 4"] =1,
["Green Hills of Stranglethorn - Page 6"] =1,
["Green Hills of Stranglethorn - Page 8"] =1,
["Green Hills of Stranglethorn - Page 10"]=1,
["Green Hills of Stranglethorn - Page 11"]=1,
["Green Hills of Stranglethorn - Page 14"]=1,
["Green Hills of Stranglethorn - Page 16"]=1,
["Green Hills of Stranglethorn - Page 18"]=1,
["Green Hills of Stranglethorn - Page 20"]=1,
["Green Hills of Stranglethorn - Page 21"]=1,
["Green Hills of Stranglethorn - Page 24"]=1,
["Green Hills of Stranglethorn - Page 25"]=1,
["Green Hills of Stranglethorn - Page 26"]=1,
["Green Hills of Stranglethorn - Page 27"]=1,
}
local neverLoot = {
    ["Fish Oil"] = 1,
    ["Nightcrawler"] = 1,

}
local bagLoot = {
["Small Brown Pouch"]=1,
["Small Blue Pouch"]=1,
["Small Black Pouch"]=1,
["Red Leather Bag"]=1,
["Large Red Sack"]=1,
["Large Knapsack"]=1,
["Large Green Sack"]=1,
["Large Brown Sack"]=1,
["Large Blue Sack"]=1,
["Journeyman's Backpack"]=1,
["Green Leather Bag"]=1,
["Blue Leather Bag"]=1,
["Traveler's Backpack"]=1,
}
local function ShouldLoot(name)
    
    -- special case handling for stv pages. Don't loot one if we have in bank,
    -- but dont have in inventory
    if stvPages[name] ~= nil then
        local countBags = GetItemCount(name)
        local countTotal = GetItemCount(name, true)
        if countBags == 0 and countTotal > 0 then
            return false
        end
        return true
    elseif bagLoot[name] ~= nil then
        CLGuide_SetupItemButton(name)
        GuideWarning("NEW BAG DROP!")
        return true
    elseif neverLoot[name] ~= nil then
        return false
    end    
    
    return true
end

local function FastLoot()
    if IsAltKeyDown() then return end
    if CLGuide_Options.EnableFastLoot == false then return end

	for i = GetNumLootItems(), 1, -1 do
        local _,name = GetLootSlotInfo(i);
        if ShouldLoot(name) then
		    LootSlot(i)
        else
            print("Lootfilter-ignoring "..name)
        end
	end
end

-- Event frame
local faster = CreateFrame("Frame")
faster:RegisterEvent("LOOT_READY")
faster:SetScript("OnEvent", FastLoot)
