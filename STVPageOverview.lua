-- Author      : G3m7
-- Create Date : 6/10/2019 12:03:58 PM
-- GetItemCount(11285) CAN BE USED to see whats in bank even lol. dont need to have savedvars and all this shit

local Quests = {
    {   
        questid=339, questname="Chapter I", 
        pages={
            {id=2725,name="Green Hills of Stranglethorn - Page 1"},
            {id=2728,name="Green Hills of Stranglethorn - Page 4"},
            {id=2730,name="Green Hills of Stranglethorn - Page 6"},
            {id=2732,name="Green Hills of Stranglethorn - Page 8"},
        }
    },
    {   
        questid=340, questname="Chapter II", 
        pages={
            {id=2734,name="Green Hills of Stranglethorn - Page 10"},
            {id=2735,name="Green Hills of Stranglethorn - Page 11"},
            {id=2738,name="Green Hills of Stranglethorn - Page 14"},
            {id=2740,name="Green Hills of Stranglethorn - Page 16"},
        }
    },
    {   
        questid=341, questname="Chapter III", 
        pages={
            {id=2742,name="Green Hills of Stranglethorn - Page 18"},
            {id=2744,name="Green Hills of Stranglethorn - Page 20"},
            {id=2745,name="Green Hills of Stranglethorn - Page 21"},
            {id=2748,name="Green Hills of Stranglethorn - Page 24"},
        }
    },
    {   
        questid=342, questname="Chapter IV", 
        pages={
            {id=2749,name="Green Hills of Stranglethorn - Page 25"},
            {id=2750,name="Green Hills of Stranglethorn - Page 26"},
            {id=2751,name="Green Hills of Stranglethorn - Page 27"},
        }
    },
}

function PrintStvPageOverview()
    for i,quest in ipairs(Quests) do
        print(quest.questname)
        for j,page in ipairs(quest.pages) do
            local countBags = GetItemCount(page.id)
            local countTotal = GetItemCount(page.id, true)
            if countBags > 0 then
                print("\124cFF00E600 "..page.name)
            elseif countTotal > 0 then
                print("\124cFFFF6D00 "..page.name.. " (bank)")
            else
                print("\124cFFFF0000 "..page.name)
            end
        end
    end
end

local function GetCompletedIds()
    local ret = {}
    for i,quest in ipairs(Quests) do
        local anyMissingInSet = false
        for j,page in ipairs(quest.pages) do
            local countBags = GetItemCount(page.id)
            local countTotal = GetItemCount(page.id, true)
            if countTotal == 0 then
                anyMissingInSet = true
            end
        end
        if anyMissingInSet == false then
            for j,page in ipairs(quest.pages) do
                ret[page.id] = 1
            end
        end
    end
    return ret
end

local function incr(val)
    print(val)
    if val == -1 then 
        return 5
    else
        return val+1
    end
end
function TakeCompleteFromBank()
    local CompletedSetIds = GetCompletedIds()
    for bag=-1, 12, incr(bag) do
        print(bag)
        for slot=1, GetContainerNumSlots(bag) do
            local itemId = GetContainerItemID(bag,slot)
            if itemId ~= nil and CompletedSetIds[itemId] ~= nil then
                UseContainerItem(bag,slot)
            end
        end
    end
end


local Requirements = {
    ["Chapter I"] = 4,
    ["Chapter II"] = 4,
    ["Chapter III"] = 4,
    ["Chapter IV"] = 3,
}
CLGuide_STVPages = {
    [2725] = {count=0, quest=339, questname="Chapter I", itemname="Green Hills of Stranglethorn - Page 1"},
    [2728] = {count=0, quest=339, questname="Chapter I", itemname="Green Hills of Stranglethorn - Page 4"},
    [2730] = {count=0, quest=339, questname="Chapter I", itemname="Green Hills of Stranglethorn - Page 6"},
    [2732] = {count=0, quest=339, questname="Chapter I", itemname="Green Hills of Stranglethorn - Page 8"},
    
    [2734] = {count=0, quest=340, questname="Chapter II", itemname="Green Hills of Stranglethorn - Page 10"},
    [2735] = {count=0, quest=340, questname="Chapter II", itemname="Green Hills of Stranglethorn - Page 11"},
    [2738] = {count=0, quest=340, questname="Chapter II", itemname="Green Hills of Stranglethorn - Page 14"},
    [2740] = {count=0, quest=340, questname="Chapter II", itemname="Green Hills of Stranglethorn - Page 16"},
    
    [2742] = {count=0, quest=341, questname="Chapter III", itemname="Green Hills of Stranglethorn - Page 18"},
    [2744] = {count=0, quest=341, questname="Chapter III", itemname="Green Hills of Stranglethorn - Page 20"},
    [2745] = {count=0, quest=341, questname="Chapter III", itemname="Green Hills of Stranglethorn - Page 21"},
    [2748] = {count=0, quest=341, questname="Chapter III", itemname="Green Hills of Stranglethorn - Page 24"},
    
    [2749] = {count=0, quest=342, questname="Chapter IV", itemname="Green Hills of Stranglethorn - Page 25"},
    [2750] = {count=0, quest=342, questname="Chapter IV", itemname="Green Hills of Stranglethorn - Page 26"},
    [2751] = {count=0, quest=342, questname="Chapter IV", itemname="Green Hills of Stranglethorn - Page 27"},
}

local BankIsOpen = false

local Frame = CreateFrame("Frame")
Frame:RegisterEvent("BAG_UPDATE")
Frame:RegisterEvent("BANKFRAME_OPENED")
Frame:RegisterEvent("BANKFRAME_CLOSED")
local BankButton = nil


local function GetCompletedIdxFromBank()
    local haveCount = {}
    for i,n in pairs(CLGuide_STVPages) do
        local x = haveCount[n.questname]
        if x == nil then
            if n.count > 0 then haveCount[n.questname] = 1 end
        else
            if n.count > 0 then haveCount[n.questname] = haveCount[n.questname] + 1 end
        end
    end

    local ret = {}
    local bag = -1
    for bag=-1, -1 do
        for slot=1, GetContainerNumSlots(bag) do
            local itemId = GetContainerItemID(bag,slot)
            if itemId ~= nil then
                local pageEntry = CLGuide_STVPages[itemId]
                if pageEntry ~= nil then
                    local countEntry = haveCount[pageEntry.questname]
                    if countEntry >= Requirements[pageEntry.questname] then
                        table.insert(ret, {bag=bag,slot=slot})
                    end
                end
            end
        end
    end

    for bag=5, 12 do
        for slot=1, GetContainerNumSlots(bag) do
            local itemId = GetContainerItemID(bag,slot)
            if itemId ~= nil then
                local pageEntry = CLGuide_STVPages[itemId]
                if pageEntry ~= nil then
                    local countEntry = haveCount[pageEntry.questname]
                    if countEntry >= Requirements[pageEntry.questname] then
                        table.insert(ret, {bag=bag,slot=slot})
                    end
                end
            end
        end
    end

    return ret
end

local function ShowBankButton()
    if BankButton == nil then
        BankButton = CreateFrame("Button", nil, _G["BankCloseButton"], "UIPanelButtonTemplate")
        BankButton:SetPoint("TOPLEFT", _G["BankCloseButton"], "TOPRIGHT", 0, 0)
        BankButton:SetWidth(200)
        BankButton:SetHeight(40)
        BankButton:SetText("Take completed STV Page sets")
        BankButton:SetScript("OnClick", function()
            local indexes = GetCompletedIdxFromBank()
            for i,v in ipairs(indexes) do
                UseContainerItem(v.bag,v.slot)
            end
        end)

    end
    BankButton:Show()
end
local function ScanBag(bag)
    for slot = 1,GetContainerNumSlots(bag) do
        local itemId = GetContainerItemID(bag,slot)
        if itemId ~= nil then
            local pageEntry = CLGuide_STVPages[itemId]
            if pageEntry ~= nil and pageEntry.count ~= 1 then
                pageEntry.count = 1
                print("have "..pageEntry.itemname)
            end
		end
	end
end

Frame:SetScript("OnEvent", function(self, event, ...)
    if event == "BANKFRAME_OPENED" then
        ScanBag(-1)
        for i=6,12 do
            if GetContainerNumSlots(i) > 0 then ScanBag(i) end
        end

        ShowBankButton()
    elseif event == "BANKFRAME_CLOSED" then
        BankButton:Hide()
    elseif event == "BAG_UPDATE" then
        local bag = tonumber(select(1,...))
        local bagname = GetBagName(bag)
        if bagname == nil then return end
        if string.find(bagname, "Quiver") or string.find(bagname, "Soul") then
            return
        end
        ScanBag(bag)
    end
end)
