--==========
--Functions
--==========
function CLGuide_GetNumFreeBagspace()
	local numFreeBagspace = 0
	for bag=0,4 do
		local bagname = GetBagName(bag)
		if bagname ~= nil then
			local bagnamelower = string.lower(bagname)
			-- ignoring soulbags and quivers
			if string.find(bagnamelower, "quiver") == nil and string.find(bagnamelower, "soul") == nil then
				for slot=1,GetContainerNumSlots(bag)  do
					if GetContainerItemInfo(bag, slot) == nil then
						numFreeBagspace = numFreeBagspace + 1
					end
				end
			end
		end
	end
	return numFreeBagspace
end

-- Return index of a particular gossip type (binder, taxi, etc) on an NPC. If none is found, return nil
function CLGuide_GetGossipIndex(type) 
	local gossipOptions = {GetGossipOptions()} -- TODO: Make sure GetGossipOptions() returns same num of args on retail (2 in 1.12)
    
	for i=1,table.getn(gossipOptions) do
		if gossipOptions[(i*2)] == type then
			return i
		end
	end
	return nil
end

-- Return index of a gossip index, if that gossips text contains str
function CLGuide_GetGossipIndexFromStringMatch(str)
    local gossipOptions = {GetGossipOptions()} -- TODO: Make sure GetGossipOptions() returns same num of args on retail (2 in 1.12)
	for i=1,(table.getn(gossipOptions)/2) do
        local text = string.lower(gossipOptions[(i*2)-1])
        if string.find(text, str) then 
            return i
        end
	end
	return nil
end

function CLGuide_GetNumStrings(data)
	if data == nil then return 0 end
	local ret = 0
	for i=1,getn(data) do
		if type(data[i]) == "string" then 
			ret = ret + 1 
		end
	end
	return ret
end

function CLGuide_GetItemInventoryCount(itemName) -- itemName, not link
	local count = 0
	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local item = GetContainerItemLink(bag,slot)
			if item ~= nil and string.find(item,itemName) then
				local _,slotCount = GetContainerItemInfo(bag, slot);
				count = slotCount + count
			end
		end
	end
	return count
end

function CLGuide_GetInventoryItemInfo(itemName)
    for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local item = GetContainerItemLink(bag,slot)
			if item ~= nil and string.find(item,itemName) then
				local texture, count = GetContainerItemInfo(bag, slot)
                return bag, slot, texture, count
			end
		end
	end
    return nil
end


function CLGuide_HaveQuestInQuestlog(qtitle)
	local e = GetNumQuestLogEntries()
    local qlower = string.lower(qtitle)
	for q=1, e do
		if string.lower(GetQuestLogTitle(q)) == qlower then 
			return 1
		end
	end
	--QRP_Print("CL_HasQuest: <"..qtitle.."> not found")
	return 0
end

function CLGuide_PlaceSpell(spellname, actionBarIndex)
    PickupSpellBookItem(spellname)
    PlaceAction(actionBarIndex)
    ClearCursor()
end

function CLGuide_GetMacroBarIdx(macroname)
    for i=1, getn(CLGuide_MacroTable) do
        if CLGuide_MacroTable[i][1] == macroname then
            return CLGuide_MacroTable[i][5]
        end
    end
end

function CLGuide_PlaceMacro(macroname)
    local macroIdx = CLGuide_GetMacroBarIdx(macroname)
    local existingText = GetActionText(macroIdx)
    if existingText ~= nil and existingText == macroname then return end
    ClearCursor()
    PickupMacro(GetMacroIndexByName(macroname))
    PlaceAction(macroIdx)
    ClearCursor()
end

function CLGuide_RePlaceSpells()
    if CLGuide_SpellTable == nil then return end
    local myLvl = UnitLevel("player")
    for lvl=1, getn(CLGuide_SpellTable) do
        local lvlEntry = CLGuide_SpellTable[lvl]
        if lvlEntry.Lvl <= myLvl then
            for i,spell in ipairs(lvlEntry.Skills) do
                if spell.Macro ~= nil then
                    CLGuide_PlaceMacro(spell.Macro)
                elseif spell.Actionbar ~= nil then
                    CLGuide_PlaceSpell(spell.n, spell.Actionbar)
                end
            end
        end
    end
end



