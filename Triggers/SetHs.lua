
function CLGuide_SetHs(self, event)
	if CLGuide_CurrentStepTable.SetHs == nil then return end
	if IsShiftKeyDown() then return end

	if event == "GOSSIP_SHOW" then 
        if UnitName("target") ~= CLGuide_CurrentStepTable.SetHs then return end
        local idx = CLGuide_GetGossipIndex("binder")
        if idx ~= nil then
            SelectGossipOption(idx)
        end
    elseif event == "CONFIRM_BINDER" then
        if UnitName("target") ~= CLGuide_CurrentStepTable.SetHs then return end
	    ConfirmBinder()
        CLGuide_CompleteCurrentStep()
    end

end