
local CLGuide_TaxiMapIsOpen = false

function CLGuide_TakeTaxi(self, event)
	if CLGuide_CurrentStepTable.Taxi == nil then return end
	if IsShiftKeyDown() then return end -- disabling taxi logic when holding shift down
    if CLGuide_Options.EnableAutomaticTaxi == false then return end

	-- todo: handle CLGuide_CurrentStepTable.ReqPrev. Dont wanna fly away automatically if we havent completed prevstep!
	
	if event == "GOSSIP_SHOW" then
		local taxiIdx = CLGuide_GetGossipIndex("taxi")
		if taxiIdx ~= nil then
            if IsMounted() then
               pcall(Dismount)
            end
			-- TODO: CHECK THAT LAST N STEPS ARE PROPERLY COMPLETED BEFORE FLYING AWAY!!!
			-- ALTERNATIVELY, or additionally, popupbox with confirm button 
			SelectGossipOption(taxiIdx)
		end
    elseif event == "TAXIMAP_OPENED" then
        CLGuide_TaxiMapIsOpen = true
    elseif event == "TAXIMAP_CLOSED" then
        CLGuide_TaxiMapIsOpen = false
    end
end

function CLGuide_TaxiUpdate()
    if CLGuide_TaxiMapIsOpen == true then
        if CLGuide_CurrentStepTable.Taxi == nil then return end
        if GetUnitSpeed("player") == 0 and IsMounted() == false then
            CLGuide_TaxiMapIsOpen = false
            --GuidePrint("Checking fps")
            for i=1, NumTaxiNodes() do
			    if string.lower(TaxiNodeName(i)) == string.lower(CLGuide_CurrentStepTable.Taxi) then
                    GuideWarning("Flying to: "..TaxiNodeName(i))
                    TaxiNodeOnButtonEnter(_G["TaxiButton"..i])
			    	TakeTaxiNode(i)
                    -- todo: if you are moving, TakeTaxiNode() will fail. We should use IsUnitOnFlight("player") (or something like that) to see if you are actually flying before continuing
			    	CLGuide_CompleteCurrentStep()
			    	return
			    end
		    end
            GuideWarning("Taxi Destination not found: "..CLGuide_CurrentStepTable.Taxi)
        end
    end
end