
function CLGuide_ResetTalents(self, event, arg1)
    if CLGuide_CurrentStepTable.ResetTalents == nil then return end

    if event == "GOSSIP_SHOW" then
        local idx = CLGuide_GetGossipIndexFromStringMatch("unlearn my talents")
        if idx ~= nil then
            SelectGossipOption(idx)
        else
            idx = CLGuide_GetGossipIndexFromStringMatch("yes. i do.")
            if idx ~= nil then
                SelectGossipOption(idx)
            end
        end
    elseif event == "CONFIRM_TALENT_WIPE" then
        local cost = tonumber(arg1)
        if cost > 10000 then 
            GuideWarning("Not auto-resetting talents, cost is > 1g ("..cost..")")
            return
        end
        GuideWarning("Resetting Talents")
        ConfirmTalentWipe()
        CLGuide_CompleteCurrentStep()
    end
end
