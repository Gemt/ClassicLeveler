-- Author      : G3m7
-- Create Date : 5/10/2019 7:19:02 PM
local function GetAllItemsInBag(itemName)
    local items = {}
    for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local item = GetContainerItemLink(bag,slot)
			if item ~= nil and string.find(item,itemName) then
                table.insert(items, {b=bag,s=slot,l=item})
			end
		end
	end
    return items
end
local function PutItemInBank(item)
    local itemIndexes = GetAllItemsInBag(item)
    for x=1,getn(itemIndexes) do
        GuideWarning("Banking: "..itemIndexes[x].l)
        UseContainerItem(itemIndexes[x].b, itemIndexes[x].s)
    end
end
local function PutItemsInBank(items)
    for i=1,getn(items) do
        PutItemInBank(items[i])
    end
end

local function incr(val)
    if val == -1 then 
        return 5
    else
        return val+1
    end
end
local function TakeItemFromBank(item)
    -- todo: bag checks dont work?
    for bag=-1, -1 do
        print("checking "..bag)
        for slot=1, GetContainerNumSlots(bag) do
            local link = GetContainerItemLink(bag,slot)
            if link ~= nil and string.find(link,item) ~= nil then
                UseContainerItem(bag,slot)
            end
        end
    end
    for bag=5, 12 do
        print("checking "..bag)
        for slot=1, GetContainerNumSlots(bag) do
            local link = GetContainerItemLink(bag,slot)
            if link ~= nil and string.find(link,item) ~= nil then
                UseContainerItem(bag,slot)
            end
        end
    end
end

local function TakeItemsFromBank(items)
    for i=1,getn(items) do
        TakeItemFromBank(items[i])        
    end
end

function CLGuide_HandleBanking(self, event)
    if CLGuide_CurrentStepTable.PutInBank == nil and CLGuide_CurrentStepTable.TakeFromBank == nil then return end
    if CLGuide_Options.EnableAutomaticBanking == false then return end

    if event == "GOSSIP_SHOW" then
        local bankIdx = CLGuide_GetGossipIndex("banker")
        if bankIdx ~= nil then
            SelectGossipOption(bankIdx)
        end
    elseif event == "BANKFRAME_OPENED" then
        if CLGuide_CurrentStepTable.PutInBank ~= nil then
            PutItemsInBank(CLGuide_CurrentStepTable.PutInBank)
        end
        if CLGuide_CurrentStepTable.TakeFromBank ~= nil then
            TakeItemsFromBank(CLGuide_CurrentStepTable.TakeFromBank)
        end
        CLGuide_CompleteCurrentStep()
    end
end