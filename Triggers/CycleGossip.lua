
function CLGuide_CycleGossip(self, event)

	if IsShiftKeyDown() then return end
    if CLGuide_CurrentStepTable.CycleGossip == nil then return end
    if event == "GOSSIP_SHOW" then 
        local talkIndex = CLGuide_GetGossipIndex("gossip")
        SelectGossipOption(talkIndex)
    elseif event == "QUEST_LOG_UPDATE" then
        if CLGuide_IsQuestComplete(CLGuide_CurrentStepTable.CycleGossip) == 1 then
    		CLGuide_CompleteCurrentStep()
    	end
    end

end