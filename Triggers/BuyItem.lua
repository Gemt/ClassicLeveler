
local function OnMerchantShow()
	--GuidePrint(CLGuide_CurrentStepTable.BuyItem.Item..", "..CLGuide_CurrentStepTable.BuyItem.Npc..", "..UnitName("target"))
    -- with the new delayed logic, this function can run after we have gone next step it seems
    if CLGuide_CurrentStepTable.BuyItem == nil then return end

	for i=1, GetMerchantNumItems() do
		local itmName,_,price,quantity = GetMerchantItemInfo(i)
        local pricePerItem = price/quantity
        if itmName ~= nil and itmName == CLGuide_CurrentStepTable.BuyItem.Item then
	        local invCount = CLGuide_GetItemInventoryCount(CLGuide_CurrentStepTable.BuyItem.Item)
	        if invCount < CLGuide_CurrentStepTable.BuyItem.Count then
	            local itemCountToBuy = CLGuide_CurrentStepTable.BuyItem.Count - invCount
			    -- stackable items like arrows cant be bought one by one (as in, you cant get less than 200)
			    -- but we let the itemCountToBuy/quantity round down to never buy MORE items than specified
			    -- TODO: after testing on classsic, you seem to be able to buy exact amounts, and we should not need to divide by quantity
                --itemCountToBuy = itemCountToBuy/quantity
			    if itemCountToBuy*pricePerItem > GetMoney() then
				    GuideWarning("You do not have enough money to buy the required items")
				    return
                end
                if string.find(CLGuide_CurrentStepTable.BuyItem.Item, " Arrow") ~= nil then
                    -- can only buy 200 arrows at a time, so yea
                    local stacksToBuy = math.ceil(itemCountToBuy/200)
                    for x=1, stacksToBuy do
                        BuyMerchantItem(i, 200)
                    end
                else
				    BuyMerchantItem(i, itemCountToBuy)
                end
			    CLGuide_CompleteCurrentStep()
                GuideWarning("Bought "..tostring(itemCountToBuy).." "..itmName)
                return;
		    else
                GuideWarning("Already have "..invCount.."/"..CLGuide_CurrentStepTable.BuyItem.Count.." of "..CLGuide_CurrentStepTable.BuyItem.Item)
                CLGuide_CompleteCurrentStep()
                return
            end
        end
	end
    GuideWarning("COULD NOT FIND <"..CLGuide_CurrentStepTable.BuyItem.Item.."> ON VENDOR")
    GuideWarning("Hold SHIFT while re-opening the vendor to prevent the window from automatically closing.")
    CloseMerchant()
end

local waitingToBuy = 0

function CLGuide_BuyItem(self, event) 
	if CLGuide_CurrentStepTable.BuyItem == nil then return end
	--if UnitName("target") ~= CLGuide_CurrentStepTable.BuyItem.Npc then return end
    if IsShiftKeyDown() then return end

	if event == "MERCHANT_SHOW" then
        waitingToBuy = 1
	elseif event == "GOSSIP_SHOW" then
        local idx = CLGuide_GetGossipIndex("vendor")
        if idx ~= nil then
		    SelectGossipOption(idx)
        end
	elseif event == "BAG_UPDATE" then
        if waitingToBuy == 1 then

        end
    end
end

function CLGuide_BuyItemOnUpdate()
    if waitingToBuy == 0 then return end
    waitingToBuy = 0
	OnMerchantShow()
end