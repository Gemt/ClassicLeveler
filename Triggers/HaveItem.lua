
local LootedNewStepItem = false
function CLGuide_HaveItem(self, event, arg1)
	if CLGuide_CurrentStepTable.Item == nil then return end

	if event == "CHAT_MSG_LOOT" then
        local steplower = string.lower(CLGuide_CurrentStepTable.Item.Name)
        local arg1lower = string.lower(arg1)
	    if string.find(arg1lower, steplower) ~= nil then
            LootedNewStepItem = true
        end
	elseif event == "BAG_UPDATE" then
        --if LootedNewStepItem == true then
            --LootedNewStepItem = false
            local inventoryCount = CLGuide_GetItemInventoryCount(CLGuide_CurrentStepTable.Item.Name)
		    --GuidePrint("the looted item was stepitem, have "..inventoryCount.."/"..CLGuide_CurrentStepTable.Item.Count)
            if inventoryCount >= CLGuide_CurrentStepTable.Item.Count then
			    CLGuide_CompleteCurrentStep()
                return 1
		    end
        --end
    end
end