
function CLGuide_DeathWarp(self, event)
    if CLGuide_CurrentStepTable.DeathWarp == nil then return end
	if IsShiftKeyDown() then return end

	if event == "GOSSIP_SHOW" then 
        local healerIndex = CLGuide_GetGossipIndex("healer")
        if healerIndex ~= nil then
            SelectGossipOption(healerIndex)
        end
    elseif event == "CONFIRM_XP_LOSS" then
	    AcceptXPLoss()
        CLGuide_CompleteCurrentStep()
    elseif event == "PLAYER_DEAD" then
        RepopMe()
    end
end