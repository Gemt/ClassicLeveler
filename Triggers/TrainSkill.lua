
CLGuide_PlaceSpellQueue = {}

-- automatically finds the highest rank of the spell
-- not needed in classic as of now.
function CLGuide_GetSpellBookId(spellname)
    local a = 1
    local b = GetSpellBookItemName(a, BOOKTYPE_SPELL)
    local c = nil
    while b do
	    if b == spellname and GetSpellBookItemName(a+1, BOOKTYPE_SPELL) ~= spellname then 
            c = a
		end
        a = a + 1
        b = GetSpellBookItemName(a, BOOKTYPE_SPELL)
	end
	return c
end    

local function CLGuide_GetSkillInfo(inName, inRank)
    SetTrainerServiceTypeFilter("available", 1);
    SetTrainerServiceTypeFilter("unavailable", 0);
    SetTrainerServiceTypeFilter("used", 1);
    for spellIdx=1, GetNumTrainerServices() do
        local name, rank, category, expanded = GetTrainerServiceInfo(spellIdx)
        if category ~= header then 
            if name == inName then
                if category == "used" and rank == inRank then
                    return -1
                elseif category == "available" and rank == inRank then
                    return spellIdx, name, rank, category, expanded
                end
            end
        end
    end
    return 0
end

local function TrainSkillsFromSkillTable(skillTable)
    local lvl = UnitLevel("player")
    local anyNotTrained = false
    local trainerType = GetTrainerServiceInfo(1) -- name of the first header
    for i=1,getn(skillTable) do
        local skillGroup = skillTable[i]
        -- only checking spells 9 levels back in time, we never go 9 levels between training
        -- or maybe just check it all? logging will be verbose af though
        if skillGroup.Lvl <= lvl then --and skillGroup.Lvl > (lvl-9) then
            for x=1,getn(skillGroup.Skills) do
                local skill = skillGroup.Skills[x]
                -- ignoring skills whose type does not match trainerType, if type is specified
                if skill.Type == nil or skill.Type == trainerType then
                    local spellIdx, name, rank, category, expanded = CLGuide_GetSkillInfo(skill.n, skill.r)
                    if spellIdx == -1 then
                        -- GuidePrint("Skill already known: "..skill.n.." ("..tostring(skill.r)..")")
                    elseif spellIdx == 0 then
                        if skill.Type ~= "Development Skills" then
                            GuideWarning("Could not find skill: "..skill.n.." ("..tostring(skill.r)..")")
                            anyNotTrained = true
                        end
                    elseif GetTrainerServiceCost(spellIdx) > GetMoney() then
                        GuideWarning("Not enough money to train: "..skill.n.." ("..tostring(skill.r)..")")
                        anyNotTrained = true
                    else
                        BuyTrainerService(spellIdx)
                        if skill.Actionbar ~= nil or skill.Macro ~= nil then
                            -- queueing delayed placement on actionbars
                            table.insert(CLGuide_PlaceSpellQueue, {Spell=skill,Time=GetTime()+2})
                        end
                    end
                end
            end
        end
    end
    return anyNotTrained
end

local nextTrainerShowToHandle = 0
local TrainerIsShown = false
local LastTrainerUpdate = 0
function CLGuide_TrainSkill(self, event)

    if event == "TRAINER_SHOW" then
        TrainerIsShown = true
        if CLGuide_Options.EnableSpellTraining == true then
            SetTrainerServiceTypeFilter("available", 1);
            SetTrainerServiceTypeFilter("unavailable", 0);
            SetTrainerServiceTypeFilter("used", 1);
        end
    elseif event == "TRAINER_CLOSED" then
        TrainerIsShown = false
    elseif event == "TRAINER_UPDATE" then
        LastTrainerUpdate = GetTime()
    end

    if CLGuide_CurrentStepTable.TrainSkill == nil then return end
    if event == "GOSSIP_SHOW" then
        local trainerIndex = CLGuide_GetGossipIndex("trainer")
        if trainerIndex ~= nil then
            SelectGossipOption(trainerIndex)
        end
    end
end

function CLGuide_TrainSkillOnUpdate()
    if TrainerIsShown == true and CLGuide_SpellTable ~= nil and IsShiftKeyDown() == false and CLGuide_Options.EnableSpellTraining == true then
        if ((LastTrainerUpdate+0.5) < GetTime()) then
            local anyNotTrained = TrainSkillsFromSkillTable(CLGuide_SpellTable)
            if anyNotTrained == false then
                TrainerIsShown = false
                GuideWarning("All spells trained")
                if CLGuide_CurrentStepTable.TrainSkill ~= nil then
                    CLGuide_CompleteCurrentStep()
                end
            else
                LastTrainerUpdate = GetTime() -- retrying in 1 sec
            end
        end
    end

    if InCombatLockdown() == true then return end
    for i=getn(CLGuide_PlaceSpellQueue), 1, -1 do
        local entry = CLGuide_PlaceSpellQueue[i]
        if GetTime() > entry.Time then
            if entry.Spell.Macro ~= nil then
                CLGuide_PlaceMacro(entry.Spell.Macro)
            else
                CLGuide_PlaceSpell(entry.Spell.n, entry.Spell.Actionbar)
            end
            table.remove(CLGuide_PlaceSpellQueue,i)
        end
    end
end