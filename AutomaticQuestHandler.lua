----------------------------------------------------------------------
-- Automate quest accept/completion while ALT key is pressed.
-- Copied from Leatrix Plus 1.13.03 (30th May 2019, www.leatrix.com)
----------------------------------------------------------------------

-- Funcion to ignore specific NPCs
local function isNpcBlocked(actionType)
	local npcGuid = UnitGUID("target") or nil
	if npcGuid then
		local void, void, void, void, void, npcID = strsplit("-", npcGuid)
		if npcID then
			-- Ignore specific NPCs for selecting, accepting and turning-in quests (required if automation has consequences)
			if npcID == "15192"	-- Anachronos (Caverns of Time)
			then
				return true
			end
			-- Ignore specific NPCs for selecting quests only (only used for items that have no other purpose)
			if actionType == "Select" then
				if npcID == "12944" -- Lokhtos Darkbargainer (Thorium Brotherhood, Blackrock Depths)
				then
					return true
				end
			end
		end
	end
end

-- Function to check if quest requires currency or a crafting reagent
local function QuestRequiresCurrency()
	for i = 1, 6 do
		local progItem = _G["QuestProgressItem" ..i] or nil
		if progItem and progItem:IsShown() and progItem.type == "required" then
			if progItem.objectType == "currency" then
				-- Quest requires currency so do nothing
				return true
			elseif progItem.objectType == "item" then
				-- Quest requires an item
				local name, texture, numItems = GetQuestItemInfo("required", i)
				if name then
					local itemID = GetItemInfoInstant(name)
					if itemID then
						local void, void, void, void, void, void, void, void, void, void, void, void, void, void, void, void, isCraftingReagent = GetItemInfo(itemID)
						if isCraftingReagent then
							-- Item is a crafting reagent so do nothing
							return true
						end
					end
				end
			end
		end
	end
end

-- Function to check if quest requires gold
local function QuestRequiresGold()
	local goldRequiredAmount = GetQuestMoneyToGet()
	if goldRequiredAmount and goldRequiredAmount > 0 then
		return true
	end
end

-- Create event frame
local qFrame = CreateFrame("FRAME")
qFrame:RegisterEvent("QUEST_DETAIL")
qFrame:RegisterEvent("QUEST_ACCEPT_CONFIRM")
qFrame:RegisterEvent("QUEST_PROGRESS")
qFrame:RegisterEvent("QUEST_COMPLETE")
qFrame:RegisterEvent("QUEST_GREETING")
qFrame:RegisterEvent("QUEST_AUTOCOMPLETE")
qFrame:RegisterEvent("GOSSIP_SHOW")
qFrame:RegisterEvent("QUEST_FINISHED")

-- Event handler
qFrame:SetScript("OnEvent", function(self, event, arg1)
	-- Clear progress items when quest interaction has ceased
	if event == "QUEST_FINISHED" then
		for i = 1, 6 do
			local progItem = _G["QuestProgressItem" ..i] or nil
			if progItem and progItem:IsShown() then
				progItem:Hide()
			end
		end
		return
	end

	-- Do nothing unless ALT key is being held
	if IsAltKeyDown() ~= true then return end

	----------------------------------------------------------------------
	-- Accept quests automatically
	----------------------------------------------------------------------

	-- Accept quests with a quest detail window
	if event == "QUEST_DETAIL" then
		-- Don't accept blocked quests
		if isNpcBlocked("Accept") then return end
		-- Accept quest
		AcceptQuest()
		HideUIPanel(QuestFrame)
	end

	-- Accept quests which require confirmation (such as sharing escort quests)
	if event == "QUEST_ACCEPT_CONFIRM" then
		ConfirmAcceptQuest() 
		StaticPopup_Hide("QUEST_ACCEPT")
	end

	----------------------------------------------------------------------
	-- Turn-in quests automatically
	----------------------------------------------------------------------

	-- Turn-in progression quests
	if event == "QUEST_PROGRESS" and IsQuestCompletable() then
		-- Don't continue quests for blocked NPCs
		if isNpcBlocked("Complete") then return end
		-- Don't continue if quest requires currency
		if QuestRequiresCurrency() then return end
		-- Don't continue if quest requires gold
		if QuestRequiresGold() then return end
		-- Continue quest
		CompleteQuest()
	end

	-- Turn in completed quests if only one reward item is being offered
	if event == "QUEST_COMPLETE" then
		-- Don't complete quests for blocked NPCs
		if isNpcBlocked("Complete") then return end
		-- Don't complete if quest requires currency
		if QuestRequiresCurrency() then return end
		-- Don't complete if quest requires gold
		if QuestRequiresGold() then return end
		-- Complete quest
		if GetNumQuestChoices() <= 1 then
			GetQuestReward(GetNumQuestChoices())
		end
	end

	-- Show quest dialog for quests that use the objective tracker (it will be completed automatically)
	if event == "QUEST_AUTOCOMPLETE" then
		local index = GetQuestLogIndexByID(arg1)
		if GetQuestLogIsAutoComplete(index) then
			ShowQuestComplete(index)
		end
	end

	----------------------------------------------------------------------
	-- Select quests automatically
	----------------------------------------------------------------------

	if event == "GOSSIP_SHOW" or event == "QUEST_GREETING" then

		-- Select quests
		if UnitExists("npc") or QuestFrameGreetingPanel:IsShown() or GossipFrameGreetingPanel:IsShown() then

			-- Don't select quests for blocked NPCs
			if isNpcBlocked("Select") then return end

			-- Select quests
			if event == "QUEST_GREETING" then
				-- Select quest greeting completed quests
				for i = 1, GetNumActiveQuests() do
					local title, isComplete = GetActiveTitle(i)
					if title and isComplete then
						return SelectActiveQuest(i)
					end
				end
				-- Select quest greeting available quests
				for i = 1, GetNumAvailableQuests() do
					local title, isComplete = GetAvailableTitle(i)
					if title and not isComplete then
						return SelectAvailableQuest(i)
					end
				end
			else
				-- Select gossip completed quests
				for i = 1, GetNumGossipActiveQuests() do
					local title, level, isTrivial, isComplete, isLegendary, isIgnored = select(i * 6 - 5, GetGossipActiveQuests())
					if title and isComplete then
						return SelectGossipActiveQuest(i)
					end
				end
				-- Select gossip available quests
				for i = 1, GetNumGossipAvailableQuests() do
					local title, level, isTrivial, isDaily, isRepeatable, isLegendary, isIgnored = select(i * 7 - 6, GetGossipAvailableQuests())
					if title then
						return SelectGossipAvailableQuest(i)
					end
				end
			end
		end
	end

    -- in case none of the above, use alt-key to automate gossip-toggling, training etc
    if event == "GOSSIP_SHOW" then
        if GetNumGossipOptions() > 0 then
            return SelectGossipOption(1)
        end
    end
end)
